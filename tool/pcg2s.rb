#!/usr/bin/env ruby
#
# Pcg2S.rb -- Converts .pcg files to source
#
# Copyright (C) 2012-2014 trap15 <trap15@raidenii.net>
# All Rights Reserved.
#

$verbose = false

unless ARGV.length == 4
  print "Incorrect arguments. Usage:\n"
  print "  #{$0} deffile in.pcg name out.S\n\n"
  exit
end

$deffile = ARGV.shift
require_relative 'convlib/conv.rb'

def emitPCGSource(pcg, name)
  pcgdat_label = sprintf "_pcgdata_%s", name
  pcgcnt_label = sprintf "_pcgcnt_%s", name

  dat  = pcg.game.emit.emitDefine(pcgcnt_label, pcg.pcgcnt)
  dat += pcg.game.emit.emitLabel(pcgdat_label)
  dat += pcg.emitPCG

  if $verbose
    printf "%s\n", dat
  end
  dat
end

inpcg = ARGV.shift
name = ARGV.shift.sub(/[^a-zA-Z0-9]/, '_')
srcout = ARGV.shift

pcg = PcgFile.new(GameDefUse.new, inpcg)

srcfp = File.open(srcout, "w")
srcfp.printf "%s\n", emitPCGSource(pcg, name)
srcfp.close
