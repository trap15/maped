#!/usr/bin/env ruby
#
# Pcx2Pcg.rb -- Converts .pcx images to .pcg tiles
#
# Copyright (C) 2012-2014 trap15 <trap15@raidenii.net>
# All Rights Reserved.
#

$verbose = false

unless ARGV.length == 5 || ARGV.length == 6
  print "Incorrect arguments. Usage:\n"
  print "  #{$0} deffile in.pcx paloffset pcgoffset out.pcg [rxy]\n\n"
  exit
end

$deffile = ARGV.shift
require_relative 'convlib/conv.rb'

def addPCG(fn,pcg, paloff, flipx,flipy,rot)
  img = Pcx.new(fn)
  xtiles = img.w / pcg.game.pcgWidth
  ytiles = img.h / pcg.game.pcgHeight

  flipx = !flipx if pcg.game.pcgOrderXFlip?
  flipy = !flipy if pcg.game.pcgOrderYFlip?

# Load in PCG from TL to BR
  for y in 0...ytiles
    for x in 0...xtiles
      px = x
      py = y

      px = xtiles-x-1 if flipx
      py = ytiles-y-1 if flipy
      px,py = py,px if rot

      rx = px * pcg.game.pcgWidth
      ry = py * pcg.game.pcgHeight

      til = pcg.buildTile(img, rx,ry, paloff, flipx,flipy,rot)
      pcg.appendTile(til)
    end
  end
end

inpcx = ARGV.shift
paloff = ARGV.shift.to_i
pcgoff = ARGV.shift.to_i + 1
pcgout = ARGV.shift

flipx = false
flipy = false
rot = false
if ARGV.length != 0
  ARGV.shift.each_char { |c|
    if c == 'x'
      flipx = true
    elsif c == 'y'
      flipy = true
    elsif c == 'r'
      rot = true
    end
  }
end

pcg = PcgFile.new(GameDefUse.new)
pcg.offset = pcgoff

addPCG(inpcx,pcg, paloff, flipx,flipy,rot)

pcgfp = File.open(pcgout, "w")
pcg.write(pcgfp)
pcgfp.close

