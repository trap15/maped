#!/usr/bin/env ruby
#
# Map2S.rb -- Converts .map files to source
#
# Copyright (C) 2012-2014 trap15 <trap15@raidenii.net>
# All Rights Reserved.
#

$verbose = false

unless ARGV.length == 4
  print "Incorrect arguments. Usage:\n"
  print "  #{$0} deffile in.map name out_stem\n\n"
  exit
end

$deffile = ARGV.shift
require_relative 'convlib/conv.rb'

def emitMapHdr(map)
  maphdr_label = sprintf "map_%s_hdr", map.name
  tilepcgdat_label = sprintf "map_%s_tile_data", map.name
  tilepcgcnt_label = sprintf "map_%s_tile_cnt", map.name
  maptdat_label = sprintf "map_%s_tdat", map.name
  mapinit_label = sprintf "map_%s_init", map.name
  mapframe_label = sprintf "map_%s_frame", map.name
  maplyt_label = sprintf "map_%s_lyt", map.name
  mappal_label = sprintf "map_%s_pal", map.name

  hdr  = map.game.emit.emitLabel(maphdr_label)
  hdr += map.game.emit.emitFarAddr(tilepcgdat_label)
  hdr += map.game.emit.emitInt16Line(tilepcgcnt_label)
  if not map.game.fixedMapSize?
    hdr += map.game.emit.emitInt8Line([ map.w, map.h ])
  end
  hdr += map.game.emit.emitFarAddr(maptdat_label)
  hdr += map.game.emit.emitFarAddr(mapinit_label)
  hdr += map.game.emit.emitNearAddr(mapframe_label)
  hdr += map.game.emit.emitFarAddr(maplyt_label)
  hdr += map.game.emit.emitLabel(mappal_label)
  hdr += map.emitPAL

  if $verbose
    printf "%s\n", hdr
  end
  hdr
end

def emitMapTdat(map)
  maptdat_label = sprintf "map_%s_tdat", map.name

  tdat  = map.game.emit.emitLabel(maptdat_label)
  tdat += map.emitTiles

  if $verbose
    printf "%s\n", tdat
  end
  tdat
end

def emitMapPcg(map)
  tilepcgdat_label = sprintf "map_%s_tile_data", map.name
  tilepcgcnt_label = sprintf "map_%s_tile_cnt", map.name

  dat  = map.game.emit.emitDefine(tilepcgcnt_label, map.pcgcnt)
  dat += map.game.emit.emitLabel(tilepcgdat_label)
  dat += map.emitPCG

  if $verbose
    printf "%s\n", dat
  end
  dat
end

def emitMapLyt(map)
  maplyt_label = sprintf "map_%s_lyt", map.name

  lyt  = map.game.emit.emitLabel(maplyt_label)
  lyt += map.emitLyt

  if $verbose
    printf "%s\n", lyt
  end
  lyt
end

inmap = ARGV.shift
name = ARGV.shift.sub(/[^a-zA-Z0-9]/, '_')
outstem = ARGV.shift

map = MapFile.new(inmap, name, GameDefUse.new())

hdrout = outstem+"_hdr.S"
tdatout = outstem+"_tdat.S"
pcgout = outstem+"_pcg.S"
lytout = outstem+"_lyt.S"

hdrfp = File.open(hdrout, "w")
tdatfp = File.open(tdatout, "w")
pcgfp = File.open(pcgout, "w")
lytfp = File.open(lytout, "w")

hdrfp.printf "%s\n", emitMapHdr(map)
tdatfp.printf "%s\n", emitMapTdat(map)
pcgfp.printf "%s\n", emitMapPcg(map)
lytfp.printf "%s\n", emitMapLyt(map)

hdrfp.close
tdatfp.close
pcgfp.close
lytfp.close
