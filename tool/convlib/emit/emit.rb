#!/usr/bin/env ruby
#
# Emit.rb -- Top-level convlib emitter file.
#
# Copyright (C) 2012-2014 trap15 <trap15@raidenii.net>
# All Rights Reserved.
#

class EmitDef
# Parameters
  # Misc
  def commentChar
    ";"
  end
  # Globals
  def needsGlobal?
    true
  end
  def defGlobal(s)
    ".global " + s + "\n"
  end
  # Labels
  def sfxLabel
    ":"
  end
  def defVal(s, v)
    ".def " + s + " " + v.to_s + "\n"
  end
  # Integer notation
  def pfxHex
    "0x"
  end
  def sfxHex
    ""
  end
  def insnInt8
    ".db"
  end
  def insnInt16
    ".dw"
  end
  def insnInt32
    ".dd"
  end
  # Banking/address info
  def hasBanking?
    false
  end
  def addrWidth
    32
  end
  def bankWidth
    0
  end
  def bankOf(s)
    ""
  end

# Fixed stuff; don't change
  def initialize()
  end
  def insnIntBase(base)
    case base
      when 8
        insnInt8
      when 16
        insnInt16
      when 32
        insnInt32
      else
        insnInt32
    end
  end
  def pfxIntBase(base)
    "\t" + insnIntBase(base) + "\t"
  end
  def pfxInt8
    pfxIntBase(8)
  end
  def pfxInt16
    pfxIntBase(16)
  end
  def pfxInt32
    pfxIntBase(32)
  end
  def emitIntRaw(v, base)
    if v == nil
      v = 0
    end
    case base
      when 8
        sprintf "%02X", v
      when 16
        sprintf "%04X", v
      when 32
        sprintf "%08X", v
      else
        sprintf "%X", v
    end
  end
  def emitIntBase(v, base, len = nil, comma = nil)
    str = ""
    if v.is_a?(String)
      return v
    end

    if comma == nil
      comma = false
    end
    if len == nil
      len = v.length
    end
    for i in 0...len
      if comma
        str += ","
      else
        comma = true
      end
      str += sprintf "%s%s%s", pfxHex, emitIntRaw(v[i], base), sfxHex
    end
    str
  end

  def emitInt8(v, len = nil, comma = nil)
    emitIntBase(v, 8, len, comma)
  end
  def emitInt16(v, len = nil, comma = nil)
    emitIntBase(v, 16, len, comma)
  end
  def emitInt32(v, len = nil, comma = nil)
    emitIntBase(v, 32, len, comma)
  end
  def emitIntBaseLine(v, base, len = nil, comma = nil)
    pfxIntBase(base) + emitIntBase(v, base, len, comma) + "\n"
  end
  def emitInt8Line(v, len = nil, comma = nil)
    emitIntBaseLine(v, 8, len, comma)
  end
  def emitInt16Line(v, len = nil, comma = nil)
    emitIntBaseLine(v, 16, len, comma)
  end
  def emitInt32Line(v, len = nil, comma = nil)
    emitIntBaseLine(v, 32, len, comma)
  end
  def emitNearAddr(s)
    pfxIntBase(addrWidth) + s + "\n"
  end
  def emitFarAddr(s)
    if hasBanking?
      pfxIntBase(bankWidth) + bankOf(s) + "\n" + emitNearAddr(s)
    else
      emitNearAddr(s)
    end
  end
  def emitAddr(s)
    emitFarAddr(s)
  end
  def emitLabel(s)
    if needsGlobal?
      defGlobal(s) + s + sfxLabel + "\n"
    else
      s + sfxLabel + "\n"
    end
  end
  def emitDefine(s,v)
    if needsGlobal?
      defGlobal(s) + defVal(s,v)
    else
      defVal(s,v)
    end
  end
  def emitComment(s)
    if commentChar != nil and commentChar != ""
      commentChar + " " + s.to_s + "\n"
    else
      ""
    end
  end
end

require_relative 'emitters.rb'

