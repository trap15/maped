#!/usr/bin/env ruby
#
# gas_arm7.rb -- Emitter for GAS's ARM7 assembler
#
# Copyright (C) 2012-2014 trap15 <trap15@raidenii.net>
# All Rights Reserved.
#

class EmitGAS_ARM7 < EmitDef
# Parameters
  # Misc
  def commentChar
    "@"
  end
  # Globals
  def needsGlobal?
    true
  end
  def defGlobal(s)
    ".global " + s + "\n"
  end
  # Labels
  def sfxLabel
    ":"
  end
  def defVal(s, v)
    ".set " + s + ", " + v.to_s + "\n"
  end
  # Integer notation
  def pfxHex
    "0x"
  end
  def sfxHex
    ""
  end
  def insnInt8
    ".byte"
  end
  def insnInt16
    ".short"
  end
  def insnInt32
    ".int"
  end
  # Banking/address info
  def hasBanking?
    false
  end
  def addrWidth
    32
  end
  def bankWidth
    0
  end
  def bankOf(s)
    ""
  end
end
