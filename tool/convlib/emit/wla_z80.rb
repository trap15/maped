#!/usr/bin/env ruby
#
# wla_z80.rb -- Emitter for WLA's Z80 assembler
#
# Copyright (C) 2012-2014 trap15 <trap15@raidenii.net>
# All Rights Reserved.
#

class EmitWLAZ80 < EmitDef
# Parameters
  # Misc
  def commentChar
    ";"
  end
  # Globals
  def needsGlobal?
    false
  end
  def defGlobal(s)
    ""
  end
  # Labels
  def sfxLabel
    ":"
  end
  def defVal(s, v)
    ".def " + s + " " + v.to_s + "\n"
  end
  # Integer notation
  def pfxHex
    "$"
  end
  def sfxHex
    ""
  end
  def insnInt8
    ".db"
  end
  def insnInt16
    ".dw"
  end
  def insnInt32
    ".dd"
  end
  # Banking/address info
  def hasBanking?
    true
  end
  def addrWidth
    16
  end
  def bankWidth
    8
  end
  def bankOf(s)
    ":" + s + ""
  end
end
