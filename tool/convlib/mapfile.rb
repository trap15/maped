#!/usr/bin/env ruby
#
# Mapfile.rb -- Useful .map file abstractions.
#
# Copyright (C) 2012-2014 trap15 <trap15@raidenii.net>
# All Rights Reserved.
#

class MapFile
  attr_accessor :pal, :map, :tiles, :pcg, :tilecnt, :pcgcnt, :w, :h, :px,:py
  attr_accessor :game, :name
  def initialize(fname, name, game)
    @f = File.open(fname, "rb")
    @name = name
    @game = game
    read()
    @f.close
  end

  def read
    if @f.read(4) != "HMAP"
      raise 'Not a valid MAP file'
    end

    @tilecnt = @f.read(1).unpack("C")[0] + 1
    @pcgcnt = @f.read(2).unpack("n")[0] + 1
    @w = @f.read(1).unpack("C")[0] + 1
    @h = @f.read(1).unpack("C")[0] + 1

    @tiles = Array.new(@tilecnt)
    @pcg = Array.new(@pcgcnt)
    @map = Array.new(@h)
    @pal = Array.new(@game.palCount)

    readTiles
    readMap
    readPCG
    readPAL
  end

# Load tiles
  def readTiles
    for t in 0...@tilecnt
      @tiles[t] = @f.read(@game.tileSize).unpack("C" * @game.tileSize)
    end
  end

# Load Map
  def readMap
    for y in 0...@h
      @map[y] = @f.read(@w).unpack("C"*@w)
    end
  end

# Load PCG
  def readPCG
    for p in 0...@pcgcnt
      @pcg[p] = Array.new(@game.pcgHeight)
      for y in 0...@game.pcgHeight
        @pcg[p][y] = @f.read(@game.pcgPitch).unpack("C" * @game.pcgPitch)
      end
    end
  end

# Load PAL
  def readPAL
    for p in 0...@game.palCount
      @pal[p] = @f.read(@game.palSize).unpack(@game.palFmt * @game.palSize)
    end
  end


  def emitPAL
    dat = ""
    for p in 0...@game.palCount
      dat += @game.emit.emitIntBaseLine(@pal[p], @game.palDepth)
    end

    dat
  end

  def emitTiles
    dat = ""
    for t in 0...@tilecnt
      dat += @game.emit.emitInt8Line(@tiles[t])
    end

    dat
  end

  def emitPCG
    dat = ""
    for p in 0...@pcgcnt
      dat += @game.emit.emitComment(p)
      for y in 0...@game.pcgHeight
        dat += @game.emit.emitInt8Line(@pcg[p][y])
      end
      dat += "\n"
    end

    dat
  end

  def emitLyt
    dat = ""
    for y in 0...@h
      dat += @game.emit.emitInt8Line(@map[y])
    end

    dat
  end
end
