#!/usr/bin/env ruby
#
# Piyo_def.rb -- Game definition for Piyo
#
# Copyright (C) 2012-2014 trap15 <trap15@raidenii.net>
# All Rights Reserved.
#

class GameDefUse < GameDef
  attr_accessor :emit
# Parameters
  # PCG stuff
  def pcgBpp
    4
  end
  def pcgWidth
    8
  end
  def pcgHeight
    8
  end
  def pcgUnitSize
    8
  end
  def pcgIsPlanar?
    true
  end
  def pcgLeftMSB?
    true
  end
  def pcgOrderXFlip?
    false
  end
  def pcgOrderYFlip?
    true
  end
  # Tile stuff
  def tileWidth
    2
  end
  def tileHeight
    2
  end
  def tileAttrSize
    1
  end
  # Palette stuff
  def palSize
    16
  end
  def palCount
    2
  end
  def palDepth
    8
  end
  # Misc stuff
  def fixedMapSize?
    false
  end

  def initialize()
    @emit = EmitWLAZ80.new
  end
end
