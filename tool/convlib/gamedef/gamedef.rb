#!/usr/bin/env ruby
#
# Gamedef.rb -- Top-level game definition
#
# Copyright (C) 2012-2014 trap15 <trap15@raidenii.net>
# All Rights Reserved.
#

class GameDef
  attr_accessor :emit
# Parameters
  # PCG stuff
  def pcgBpp
    4
  end
  def pcgWidth
    8
  end
  def pcgHeight
    8
  end
  def pcgUnitSize
    8
  end
  def pcgIsPlanar?
    true
  end
  def pcgLeftMSB?
    true
  end
  def pcgOrderXFlip?
    false
  end
  def pcgOrderYFlip?
    false
  end
  # Tile stuff
  def tileWidth
    2
  end
  def tileHeight
    2
  end
  def tileAttrSize
    1
  end
  # Palette stuff
  def palSize
    16
  end
  def palCount
    2
  end
  def palDepth
    8
  end
  # Misc stuff
  def fixedMapSize?
    false
  end

# Fixed stuff; don't change
  def initialize(emit)
    @emit = emit
  end
  # PCG stuff
  def pcgPitch
    pcgWidth * pcgBpp / pcgUnitSize
  end
  def pcgPixelPerUnit
    pcgUnitSize / pcgBpp
  end
  def pcgColorMask
    (1 << pcgBpp) - 1
  end
  # Tile stuff
  def tileSize
    tileWidth * tileHeight + tileAttrSize
  end
  # Palette stuff
  def palFmt
    case palDepth
      when 8
        "C"
      when 16
        "n"
      when 32
        "N"
    end
  end
end

require_relative 'cur_def.rb'
