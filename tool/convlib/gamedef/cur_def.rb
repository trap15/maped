#!/usr/bin/env ruby
#
# Emitters.rb -- List of emitters.
#
# Copyright (C) 2012-2014 trap15 <trap15@raidenii.net>
# All Rights Reserved.
#

if $deffile != nil
  require_relative $deffile+'.rb'
else
  require_relative 'piyo_def.rb'
end
