#!/usr/bin/env ruby
#
# Pcxfile.rb -- Useful .pcx file abstractions.
#
# Copyright (C) 2012-2014 trap15 <trap15@raidenii.net>
# All Rights Reserved.
#

class Pcx
  attr_accessor :w, :h, :pal, :data
  def initialize(fname)
    @f = File.open(fname, "rb")
    @pal = Array.new(256, Array.new(3, 0))
    read()
    close()
  end

  def close
    @f.close
  end

  def read
    pcxhead = @f.read(16).unpack("CCCCS<S<S<S<S<S<")
    pal16 = @f.read(48).unpack("C"*3*16)
    @bpp = 0
    @f.read(64) # The rest of the header doesn't matter

    # Parse the header
    if(pcxhead[0] != 0x0A)
      raise 'Bad PCX file: ID'
    end
    if(pcxhead[1] != 5)
      raise 'Bad PCX file: only v3.0 supported'
    end
    if(pcxhead[2] != 1)
      raise 'Bad PCX file: encoding'
    end
    @bpp = pcxhead[3]
    unless @bpp == 8
      raise 'Bad PCX file: BPP unsupported'
    end
    @w = (pcxhead[6] - pcxhead[4]) + 1
    @h = (pcxhead[7] - pcxhead[5]) + 1

    @data = Array.new(@w * @h, 0)
    readData

    read8bppPal if @bpp == 8
  end

  def readData
    ptr = 0
    for y in 0...@h
      x = 0
      while x < @w
        byte = @f.read(1).unpack("C")[0]
        if ((byte & 0xC0) == 0xC0)
          runcnt = byte & 0x3F
          runval = @f.read(1).unpack("C")[0]
        else
          runcnt = 1
          runval = byte
        end
        for i in 0...runcnt
          data[ptr+i] = runval
        end
        ptr += runcnt
        x += runcnt
      end
    end
  end

  def read8bppPal
    @f.seek(-769, IO::SEEK_END)
    return unless @f.read(1).unpack("C")[0] == 0xC
    for c in 0...256
      @pal[c] = @f.read(3).unpack("CCC")
    end
  end
end
