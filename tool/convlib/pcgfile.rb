#!/usr/bin/env ruby
#
# Pcgfile.rb -- Useful .pcg file abstractions.
#
# Copyright (C) 2012-2014 trap15 <trap15@raidenii.net>
# All Rights Reserved.
#

class PcgFile
  attr_accessor :pcg, :pcgcnt, :offset
  attr_accessor :game
  def initialize(game, fname=nil)
    @game = game
    if fname == nil
      @pcgcnt = 0
      @offset = 1
      @pcg = Array.new
    else
      @f = File.open(fname, "rb")
      read()
      @f.close
    end
  end

  def read
    if @f.read(4) != "HPCG"
      raise 'Not a valid PCG file'
    end

    @pcgcnt = @f.read(2).unpack("n")[0] + 1
    @offset = @f.read(2).unpack("n")[0] + 1

    @pcg = Array.new(@pcgcnt)

    readPCG
  end

# Load PCG
  def readPCG
    for p in 0...@pcgcnt
      @pcg[p] = Array.new(@game.pcgHeight)
      for y in 0...@game.pcgHeight
        @pcg[p][y] = @f.read(@game.pcgPitch).unpack("C" * @game.pcgPitch)
      end
    end
  end

  def emitPCG
    dat = ""
    for p in 0...@pcgcnt
      dat += @game.emit.emitComment(p)
      for y in 0...@game.pcgHeight
        dat += @game.emit.emitInt8Line(@pcg[p][y])
      end
      dat += "\n"
    end

    dat
  end



  def write(fp)
    fp.write("HPCG")
    fp.write([ @pcgcnt-1 ].pack("n"))
    fp.write([ @offset-1 ].pack("n"))

    for p in 0...@pcgcnt
      for y in 0...@game.pcgHeight
        for b in 0...@game.pcgPitch
          fp.write([ @pcg[p][y][b] ].pack("C"))
        end
      end
    end
  end

  def buildPlanarTileLine(img,y, xo,yo, paloff, flipx,flipy,rot)
    line = Array.new(@game.pcgPitch)

    for b in 0...@game.pcgPitch
      v = 0
      for x in 0...@game.pcgWidth
        px = x
        py = y

        px = @game.pcgWidth-px-1 if flipx
        py = @game.pcgHeight-py-1 if flipy
        px,py = py,px if rot

        pos = (xo + px) + ((yo + py) * img.w)

        col = img.data[pos]
        col += paloff if col != 0
        col &= @game.pcgColorMask

        if @game.pcgLeftMSB?
          v |= ((col >> b) & 1) << (@game.pcgWidth-x-1)
        else
          v |= ((col >> b) & 1) << x
        end
      end
      line[b] = v
    end

    line
  end
  def buildChunkyTileLine(img,y, xo,yo, paloff, flipx,flipy,rot)
    line = Array.new(@game.pcgPitch)

    for x in 0...@game.pcgPitch
      v = 0
      for b in 0...@game.pcgPixelPerUnit
        px = x*@game.pcgPixelPerUnit + b
        py = y

        px = @game.pcgWidth-px-1 if flipx
        py = @game.pcgHeight-py-1 if flipy
        px,py = py,px if rot

        pos = (xo + px) + ((yo + py) * img.w)

        col = img.data[pos]
        col += paloff if col != 0
        col &= @game.pcgColorMask

        if @game.pcgPixelPerUnit == 1
          v = col
        elsif @game.pcgLeftMSB?
          v <<= @game.pcgBpp
          v |= col
        else
          v >>= @game.pcgBpp
          v |= col << @game.pcgBpp
        end
      end
      line[x] = v
    end

    line
  end

  def buildTile(img, xo,yo, paloff, flipx,flipy,rot)
    tile = Array.new(@game.pcgHeight)
    for y in 0...@game.pcgHeight
      if @game.pcgIsPlanar?
        tile[y] = buildPlanarTileLine(img,y, xo,yo, paloff, flipx,flipy,rot)
      else
        tile[y] = buildChunkyTileLine(img,y, xo,yo, paloff, flipx,flipy,rot)
      end
    end

    tile
  end

  def appendTile(til)
    @pcg.push(til)
    @pcgcnt += 1
  end

end
