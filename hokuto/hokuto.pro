#-------------------------------------------------
#
# Project created by QtCreator 2014-04-27T01:16:28
#
#-------------------------------------------------

QT += widgets

TARGET = hokuto
TEMPLATE = app

DEPENDPATH += .
INCLUDEPATH += .

# Input
HEADERS += \
    src/mapeditor.h \
    src/tile.h \
    src/util.h \
    src/tilelayer.h \
    src/screen.h \
    src/map.h \
    src/mapwindow.h \
    src/pcgviewer.h \
    src/tileeditor.h \
    src/paleditor.h \
    src/hokuto.h \
    src/metaeditor.h \
    src/gamedef.h
SOURCES += \
    src/main.cpp \
    src/mapeditor.cpp \
    src/tile.cpp \
    src/tilelayer.cpp \
    src/screen.cpp \
    src/map.cpp \
    src/mapwindow.cpp \
    src/pcgviewer.cpp \
    src/tileeditor.cpp \
    src/paleditor.cpp \
    src/metaeditor.cpp
