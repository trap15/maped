#ifndef PALVIEWER_H
#define PALVIEWER_H

//--- Qt includes
#include <QtGui>
#include <QtWidgets>

namespace Hokuto {
    class PalEditor : public QWidget
	{
		Q_OBJECT

	public:
        explicit PalEditor(QWidget *parent = 0);
        void updatePal();

	protected:
		void paintEvent(QPaintEvent *event);
        void mousePressEvent(QMouseEvent *event);

    protected slots:
        void valueChange(int i);

    private:
        QGridLayout* m_grid;

        QLabel *m_lbl_r;
        QSpinBox *m_spn_r;
        QLabel *m_lbl_g;
        QSpinBox *m_spn_g;
        QLabel *m_lbl_b;
        QSpinBox *m_spn_b;

        int m_sel;
        bool m_nochg; // hack
    };

    extern PalEditor *g_paled;
}

#endif // PALVIEWER_H
