//--- Qt includes
#include <QtGui>
//--- Editor includes
#include "util.h"
#include "screen.h"
#include "tile.h"
#include "tilelayer.h"

Hokuto::Screen Hokuto::g_screen;

Hokuto::PCG::PCG()
{
    for(int x=0; x < PCG_WIDTH; x++) {
        for(int y=0; y < PCG_HEIGHT; y++) {
			m_pix[x][y] = 0;
		}
	}
}

Hokuto::Screen::Screen()
{
	m_w = 512;
	m_h = 512;

	m_img = QImage(m_w, m_h, QImage::Format_ARGB32);
	m_img.fill(qRgba(0, 0, 0, 0xFF));

	m_bg0 = new Hokuto::TileLayer();

	reset();
}

void Hokuto::Screen::scroll(int x, int y)
{
	m_bg0->scroll(x, y);
}

void Hokuto::Screen::setScroll(int x, int y)
{
	m_bg0->setScroll(x, y);
}

void Hokuto::Screen::reset()
{
	for(int i=0; i < 512; i++) {
		m_pcg[i] = Hokuto::PCG();
	}

    for(int i=0; i < PAL_COUNT*PAL_SIZE; i++) {
        m_pal[i] = 0;
	}
}

void Hokuto::Screen::renderPCG(int code, int pal, QImage &img, int xo, int yo, int xs, int ys)
{
	Hokuto::PCG cg = m_pcg[code];

	xo -= xs;
	yo -= ys;

    for(int x=0; x < PCG_WIDTH; x++) {
        for(int y=0; y < PCG_HEIGHT; y++) {
			int col = cg.m_pix[x][y];
            uint pix = m_pal[pal*PAL_SIZE + col].getFullColor();
            if(x+xo < m_w && x+xo >= 0) {
                if(y+yo < m_h && y+yo >= 0) {
					img.setPixel(x+xo, y+yo, pix);
                }
            }
		}
	}
}

void Hokuto::Screen::_draw_bg(TileLayer *bg)
{
	for(int x=0; x < bg->m_w; x++) {
		for(int y=0; y < bg->m_h; y++) {
			Tile tile = bg->m_tiles[y*bg->m_w + x];
            renderPCG(tile.m_code, tile.m_pal, m_img, x*PCG_WIDTH, y*PCG_HEIGHT, bg->m_xscroll, bg->m_yscroll);
		}
	}
}

void Hokuto::Screen::update()
{
	// Clear the screen first
	m_img.fill(qRgba(0, 0, 0, 0xFF));

	_draw_bg(m_bg0);
}

QImage *Hokuto::Screen::getImage()
{
	return &m_img;
}



bool Hokuto::Screen::loadPCG(QString path)
{
	QFile file(path);
	if(!file.open(QIODevice::ReadOnly)) {
		return false;
	}
	uchar *data = file.map(0, file.size());
	file.close();
	return loadPCG(data);
}

bool Hokuto::Screen::loadPCG(void *data)
{
	int tiles, off, o;
    o = 0;
    if(read32(data, &o) != 0x48504347) { // Not a valid PCG
		return false;
	}

    tiles = read16(data, &o) + 1;
    off = read16(data, &o) + 1;

	for(int i=0; i < tiles; i++) {
        for(int y=0; y < PCG_HEIGHT; y++) {
            for(int x=0; x < PCG_WIDTH; x++) {
				m_pcg[off+i].m_pix[x][y] = 0;
			}
            if(PCG_PLANAR) {
                for(int b=0; b < PCG_BPP; b++) {
                    int px = read8(data, &o);
                    for(int x=0; x < PCG_WIDTH; x++) {
                        if(PCG_LEFT_MSB)
                            m_pcg[off+i].m_pix[x][y] |= ((px >> (PCG_WIDTH-x-1)) & 1) << b;
                        else
                            m_pcg[off+i].m_pix[x][y] |= ((px >> x) & 1) << b;
                    }
                }
            }else{
                for(int x=0; x < PCG_PITCH; x++) {
                    int px = read8(data, &o);
                    for(int b=0; b < PCG_PIXPERUNIT; b++) {
                        if(PCG_LEFT_MSB)
                            m_pcg[off+i].m_pix[x*PCG_PIXPERUNIT+b][y] = (px >> (PCG_UNIT_SIZE-((b+1)*PCG_BPP))) & PCG_MASK;
                        else
                            m_pcg[off+i].m_pix[x*PCG_PIXPERUNIT+b][y] = (px >> (b*PCG_BPP)) & PCG_MASK;
                    }
                }
            }
        }
	}
	return true;
}

bool Hokuto::Screen::savePCG(QString path)
{
	uchar *data;
	qint64 len;
	QFile file(path);
	if(!file.open(QIODevice::WriteOnly)) {
		return false;
	}
	savePCG(data, len);
	return file.write((const char*)data, len) == len;
}

bool Hokuto::Screen::savePCG(uchar* &data, qint64 &len)
{
	QByteArray buf;
	char z = 0;

    buf.append("HPCG");

	buf.append(0x1); // Tiles >> 8
	buf.append(0xFF); // Tiles & 0xFF
	buf.append(z); // Offset >> 8
	buf.append(z); // Offset & 0xFF

	for(int i=0; i < 0x200; i++) {
        for(int y=0; y < PCG_HEIGHT; y++) {
            if(PCG_PLANAR) {
                for(int b=0; b < PCG_BPP; b++) {
                    char v = 0;
                    for(int x=0; x < PCG_WIDTH; x++) {
                        if(PCG_LEFT_MSB)
                            v |= ((m_pcg[i].m_pix[x][y] >> b) & 1) << (PCG_WIDTH-x-1);
                        else
                            v |= ((m_pcg[i].m_pix[x][y] >> b) & 1) << x;
                    }
                    buf.append(v);
                }
            }else{
                for(int x=0; x < PCG_PITCH; x++) {
                    char v = 0;
                    for(int b=0; b < PCG_PIXPERUNIT; b++) {
                        if(PCG_LEFT_MSB) {
                            v <<= PCG_BPP;
                            v |= m_pcg[i].m_pix[x][y];
                        }else{
                            v >>= PCG_BPP;
                            v |= m_pcg[i].m_pix[x][y] << PCG_BPP;
                        }
                    }
                    buf.append(v);
                }
            }
		}
	}

	len = buf.size();
	data = new uchar[len];
	memcpy(data, buf.data(), len);
	return true;
}

bool Hokuto::Screen::loadPAL(QString path)
{
	QFile file(path);
	if(!file.open(QIODevice::ReadOnly)) {
		return false;
	}
	uchar *data = file.map(0, file.size());
	file.close();
	return loadPAL(data);
}

bool Hokuto::Screen::loadPAL(void *data)
{
    int o, cols, off;
    o = 0;
    if(read32(data, &o) != 0x4850414C) { // Not a valid PAL
		return false;
	}

    cols = read8(data, &o) + 1;
    off = read8(data, &o);

	for(int i = 0; i < cols; i++) {
        switch(PAL_DEPTH) {
            case 8:
                m_pal[off+i] = read8(data, &o);
                break;
            case 16:
                m_pal[off+i] = read16(data, &o);
                break;
            case 32:
                m_pal[off+i] = read32(data, &o);
                break;
        }
	}

	return true;
}

bool Hokuto::Screen::savePAL(QString path)
{
	uchar *data;
	qint64 len;
	QFile file(path);
	if(!file.open(QIODevice::WriteOnly)) {
		return false;
	}
	savePAL(data, len);
	return file.write((const char*)data, len) == len;
}

bool Hokuto::Screen::savePAL(uchar* &data, qint64 &len)
{
	QByteArray buf;
	char z = 0;

    buf.append("HPAL");

	buf.append(0x1F); // Colors
	buf.append(z); // Offset

	for(int i=0; i < 0x20; i++) {
        switch(PAL_DEPTH) {
            case 8:
                buf.append(m_pal[i]);
                break;
            case 16:
                buf.append((char)(m_pal[i] >> 8));
                buf.append((char)(m_pal[i] >> 0));
                break;
            case 32:
                buf.append((char)(m_pal[i] >> 24));
                buf.append((char)(m_pal[i] >> 16));
                buf.append((char)(m_pal[i] >> 8));
                buf.append((char)(m_pal[i] >> 0));
                break;
        }
	}

	len = buf.size();
	data = new uchar[len];
	memcpy(data, buf.data(), len);
	return true;
}
