//--- Qt includes
#include <QtGui>
//--- Editor includes
#include "util.h"
#include "screen.h"
#include "pcgviewer.h"

#define COLS 16
#define ROWS 32

#define SPLITSZ 6
#define SPLITCNT 1

#define PCGV_WIDTH  (PCG_WIDTH +2)
#define PCGV_HEIGHT (PCG_HEIGHT+2)

Hokuto::PCGViewer *Hokuto::g_pcgvw;

Hokuto::PCGViewer::PCGViewer(QWidget *parent) :
	QWidget(parent)
{
    resize(COLS*PCGV_WIDTH, ROWS*PCGV_HEIGHT + SPLITSZ*SPLITCNT);
    setMinimumSize(COLS*PCGV_WIDTH, ROWS*PCGV_HEIGHT + SPLITSZ*SPLITCNT);

	m_pal = 0;
}

void Hokuto::PCGViewer::paintEvent(QPaintEvent *event)
{
	(void)event;
	QPainter p(this);

	QImage img = QImage(8, 8, QImage::Format_ARGB32);
	for(int y=0, yp = 0; y < ROWS; y++) {
		for(int x=0, xp = 0; x < COLS; x++) {
			g_screen.renderPCG(y*COLS + x, m_pal, img);
			p.drawImage(xp,yp, img);
            xp += PCGV_WIDTH;
		}
        yp += PCGV_HEIGHT;
        if((y & 15) == 15)
            yp += SPLITSZ;
    }

	p.end();
}
