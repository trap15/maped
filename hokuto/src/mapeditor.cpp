//--- Qt includes
#include <QtGui>
//--- Editor includes
#include "util.h"
#include "screen.h"
#include "tile.h"
#include "map.h"
#include "mapeditor.h"
#include "tileeditor.h"
#include "tilelayer.h"
#include "metaeditor.h"
#include "mapwindow.h"

Hokuto::MapEditor *Hokuto::g_maped;

Hokuto::MapEditor::MapEditor(QWidget *parent) :
	QWidget(parent)
{
	resize(g_screen.m_w, g_screen.m_h);
	setMinimumSize(g_screen.m_w, g_screen.m_h);
    setMaximumSize(g_screen.m_w, g_screen.m_h);

	setFocusPolicy(Qt::StrongFocus);

	m_timer.start(1000/60, this);

	m_keys = m_rawkeys = 0;
}

void Hokuto::MapEditor::placeTile(int x, int y, int tile)
{
	g_map.m_map[y*g_map.m_w + x] = tile;
	Hokuto::setModified();
}

void Hokuto::MapEditor::mainUpdate()
{
    int scroll_spd = MAP_SCROLL_SPD;
	int scrollx=0, scrolly=0;

	if(m_keys & KEY_DOWN)  scrolly = scroll_spd;
	if(m_keys & KEY_UP)    scrolly =-scroll_spd;
	if(m_keys & KEY_RIGHT) scrollx = scroll_spd;
	if(m_keys & KEY_LEFT)  scrollx =-scroll_spd;

	if(scrollx || scrolly) {
		doScroll(scrollx, scrolly);
	}
	g_meted->updateMeta();
}

void Hokuto::MapEditor::doScroll(int x, int y)
{
	g_screen.scroll(x, y);
	g_map.scroll(x, y);
    g_mapwin->m_scrollx->setValue(g_mapwin->m_scrollx->value() + x);
    g_mapwin->m_scrolly->setValue(g_mapwin->m_scrolly->value() + y);
    doMouse();
}

void Hokuto::MapEditor::setScroll(int x, int y)
{
	g_screen.setScroll(x, y);
	g_map.setScroll(x, y);
	doMouse();
}

void Hokuto::MapEditor::setScrollX(int x)
{
	setScroll(x, g_map.m_yscroll);
}

void Hokuto::MapEditor::setScrollY(int y)
{
	setScroll(g_map.m_xscroll, y);
}

void Hokuto::MapEditor::paintEvent(QPaintEvent *event)
{
	(void)event;
	QPainter p(this);
	g_map.update();
	g_screen.update();
	p.drawImage(0, 0, *g_screen.getImage());
	p.drawImage(0, 0, *g_map.getImage());
}

void Hokuto::MapEditor::keyPressEvent(QKeyEvent *event)
{
	int handled = 0;
	if(event->key() == Qt::Key_Up   ) { m_rawkeys |= KEY_UP;    handled = 1; }
	if(event->key() == Qt::Key_Down ) { m_rawkeys |= KEY_DOWN;  handled = 1; }
	if(event->key() == Qt::Key_Left ) { m_rawkeys |= KEY_LEFT;  handled = 1; }
	if(event->key() == Qt::Key_Right) { m_rawkeys |= KEY_RIGHT; handled = 1; }
	if(event->key() == Qt::Key_Escape) { ((QMainWindow*)parent())->close(); handled = 1; }
	if(!handled)
		QWidget::keyPressEvent(event);
}

void Hokuto::MapEditor::keyReleaseEvent(QKeyEvent *event)
{
	int handled = 0;
	if(event->key() == Qt::Key_Up   ) { m_rawkeys &= ~KEY_UP;    handled = 1; }
	if(event->key() == Qt::Key_Down ) { m_rawkeys &= ~KEY_DOWN;  handled = 1; }
	if(event->key() == Qt::Key_Left ) { m_rawkeys &= ~KEY_LEFT;  handled = 1; }
	if(event->key() == Qt::Key_Right) { m_rawkeys &= ~KEY_RIGHT; handled = 1; }
	if(!handled)
		QWidget::keyReleaseEvent(event);
}

void Hokuto::MapEditor::doMouse()
{
	int tile = 0;
	if(m_mousebtn & Qt::LeftButton) {
		tile = g_tiled->m_sel;
	}
	if(m_mousebtn & (Qt::LeftButton | Qt::RightButton)) {
		int x = m_mousex;
		int y = m_mousey;
		x += g_screen.m_bg0->m_xscroll;
		y += g_screen.m_bg0->m_yscroll;
        x /= TILE_FWIDTH;
        y /= TILE_FHEIGHT;
		if((x < g_map.m_w) && (x >= 0)) {
			if((y < g_map.m_h) && (y >= 0)) {
				placeTile(x, y, tile);
			}
		}
	}
	repaint();
}

void Hokuto::MapEditor::mouseMoveEvent(QMouseEvent *event)
{
	m_mousebtn = event->buttons();
	m_mousex = event->x();
	m_mousey = event->y();
	doMouse();
}

void Hokuto::MapEditor::mousePressEvent(QMouseEvent *event)
{
	mouseMoveEvent(event);
}

void Hokuto::MapEditor::mouseReleaseEvent(QMouseEvent *event)
{
	m_mousebtn = event->buttons();
	m_mousex = event->x();
	m_mousey = event->y();
	QWidget::mouseReleaseEvent(event);
}

void Hokuto::MapEditor::timerEvent(QTimerEvent *event)
{
	if(event->timerId() != m_timer.timerId()) {
		QWidget::timerEvent(event);
		return;
	}

	m_keys = m_rawkeys;
	if((m_keys & KEY_LEFT) && (m_keys & KEY_RIGHT)) {
		m_keys &= ~(KEY_LEFT | KEY_RIGHT);
	}
	if((m_keys & KEY_UP) && (m_keys & KEY_DOWN)) {
		m_keys &= ~(KEY_UP | KEY_DOWN);
	}

	mainUpdate();
}

