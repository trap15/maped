#ifndef TILE_H
#define TILE_H

namespace Hokuto {
	struct Tile
	{
		Tile();

		int m_code;
		int m_xflip, m_yflip;
		int m_pal;
	};
}

#endif // TILE_H
