//--- Qt includes
#include <QtGui>
//--- Editor includes
#include "util.h"
#include "screen.h"
#include "tile.h"
#include "map.h"
#include "tileeditor.h"
#include "mapeditor.h"


// A lot of this has to change between games

Hokuto::TileEditor *Hokuto::g_tiled;

#define COLS 16
#define ROWS 16

#define TILEE_WIDTH  (TILE_FWIDTH+2)
#define TILEE_HEIGHT (TILE_FWIDTH+2)

Hokuto::TileEditor::TileEditor(QWidget *parent) :
	QWidget(parent)
{
	m_sel = 1;

	QWidget *pad = new QWidget();
    pad->resize(COLS*TILEE_WIDTH, ROWS*TILEE_HEIGHT);
    pad->setMinimumSize(COLS*TILEE_WIDTH, ROWS*TILEE_HEIGHT);
    setMaximumWidth(COLS*TILEE_WIDTH+10);

#define SETUP_SPIN(a,b,m,n) \
    m_lbl_##a = new QLabel(b); m_spn_##a = new QSpinBox; m_spn_##a->setRange(n,m); m_lbl_##a->setBuddy(m_spn_##a); \
    m_lbl_##a->setAlignment(Qt::AlignVCenter | Qt::AlignRight); \
	connect(m_spn_##a, SIGNAL(valueChanged(int)), this, SLOT(valueChange(int)))
#define SETUP_CHK(a,b) \
    m_lbl_##a = new QLabel(b); m_chk_##a = new QCheckBox; m_lbl_##a->setBuddy(m_chk_##a); \
    m_lbl_##a->setAlignment(Qt::AlignVCenter | Qt::AlignRight); \
    connect(m_chk_##a, SIGNAL(stateChanged(int)), this, SLOT(stateChange(int)))

    SETUP_SPIN(til[0], "TL", 255,0);
    SETUP_SPIN(til[1], "TR", 255,0);
    SETUP_SPIN(til[2], "BL", 255,0);
    SETUP_SPIN(til[3], "BR", 255,0);

    SETUP_SPIN(pal, "Palette", TILE_MAXPAL,TILE_MINPAL);

	m_grid = new QGridLayout;
    m_grid->addWidget(pad,      0, 0, 1, 8);
    m_grid->addWidget(m_lbl_til[0], 1, 0);
    m_grid->addWidget(m_spn_til[0], 1, 1);
    m_grid->addWidget(m_lbl_til[1], 1, 2);
    m_grid->addWidget(m_spn_til[1], 1, 3);
    m_grid->addWidget(m_lbl_til[2], 2, 0);
    m_grid->addWidget(m_spn_til[2], 2, 1);
    m_grid->addWidget(m_lbl_til[3], 2, 2);
    m_grid->addWidget(m_spn_til[3], 2, 3);

    m_grid->addWidget(m_lbl_pal, 3, 0, 1,4);
    m_grid->addWidget(m_spn_pal, 3, 4, 1,4);

#if CUR_GAMEDEF == GAMEDEF_PIYO
    SETUP_CHK(attr[0], "U");
    SETUP_CHK(attr[1], "L");
    SETUP_CHK(attr[2], "D");
    SETUP_CHK(attr[3], "R");

    m_grid->addWidget(m_lbl_attr[0],  1, 4);
    m_grid->addWidget(m_chk_attr[0],  1, 5);
    m_grid->addWidget(m_lbl_attr[3],  1, 6);
    m_grid->addWidget(m_chk_attr[3],  1, 7);
    m_grid->addWidget(m_lbl_attr[1],  2, 4);
    m_grid->addWidget(m_chk_attr[1],  2, 5);
    m_grid->addWidget(m_lbl_attr[2],  2, 6);
    m_grid->addWidget(m_chk_attr[2],  2, 7);
#elif CUR_GAMEDEF == GAMEDEF_GBAGE
    SETUP_CHK(attr[0], "WALL");

    m_grid->addWidget(m_lbl_attr[0],  1, 4);
    m_grid->addWidget(m_chk_attr[0],  1, 5);
#endif

	setLayout(m_grid);

	updateBox();
}

void Hokuto::TileEditor::applyTile()
{
    for(int i=0; i < TILE_SIZE; i++) {
        g_map.m_tiles[m_sel].m_tile[i].m_code = m_spn_til[i]->value();
        g_map.m_tiles[m_sel].m_tile[i].m_pal = m_spn_pal->value();
    }
    for(int i=0; i < TILE_ATTR_SIZE; i++)
        g_map.m_tiles[m_sel].m_flags[i] = 0;

#if CUR_GAMEDEF == GAMEDEF_PIYO
    g_map.m_tiles[m_sel].m_flags[0] = m_spn_pal->value() << 4;
    if(m_chk_attr[0]->isChecked()) g_map.m_tiles[m_sel].m_flags[0] |= (1<<0);
    if(m_chk_attr[1]->isChecked()) g_map.m_tiles[m_sel].m_flags[0] |= (1<<1);
    if(m_chk_attr[2]->isChecked()) g_map.m_tiles[m_sel].m_flags[0] |= (1<<2);
    if(m_chk_attr[3]->isChecked()) g_map.m_tiles[m_sel].m_flags[0] |= (1<<3);
#elif CUR_GAMEDEF == GAMEDEF_GBAGE
    g_map.m_tiles[m_sel].m_flags[1] = m_spn_pal->value();
    if(m_chk_attr[0]->isChecked()) g_map.m_tiles[m_sel].m_flags[0] |= (1<<0);
#endif

	Hokuto::setModified();
	g_maped->repaint();
	repaint();
}

void Hokuto::TileEditor::valueChange(int i)
{
	(void)i;
	applyTile();
}

void Hokuto::TileEditor::stateChange(int i)
{
	(void)i;
	applyTile();
}

void Hokuto::TileEditor::updateBox()
{
	MapTile seltile = g_map.m_tiles[m_sel];
    for(int i=0; i < TILE_SIZE; i++) {
        m_spn_til[i]->setValue(seltile.m_tile[i].m_code);
    }
#if CUR_GAMEDEF == GAMEDEF_PIYO
    m_spn_pal->setValue((seltile.m_flags[0] >> 4) & 0xF);
    m_chk_attr[0]->setChecked(seltile.m_flags[0] & (1<<0));
    m_chk_attr[1]->setChecked(seltile.m_flags[0] & (1<<1));
    m_chk_attr[2]->setChecked(seltile.m_flags[0] & (1<<2));
    m_chk_attr[3]->setChecked(seltile.m_flags[0] & (1<<3));
#elif CUR_GAMEDEF == GAMEDEF_GBAGE
    m_spn_pal->setValue(seltile.m_flags[1] & 0xF);
    m_chk_attr[0]->setChecked(seltile.m_flags[0] & (1<<0));
#endif
}

void Hokuto::TileEditor::paintEvent(QPaintEvent *event)
{
	(void)event;
	QPainter p(this);

    QImage img[TILE_SIZE];
    for(int i=0; i < TILE_SIZE; i++)
        img[i] = QImage(PCG_WIDTH, PCG_HEIGHT, QImage::Format_ARGB32);
    for(int y=0; y < ROWS; y++) {
        for(int x=0; x < COLS; x++) {
            for(int i=0; i < TILE_SIZE; i++)
                g_screen.renderPCG(g_map.m_tiles[y*COLS + x].m_tile[i].m_code, g_map.m_tiles[y*COLS + x].m_tile[i].m_pal, img[i]);

            for(int i=0,ty=0; ty < TILE_HEIGHT; ty++) {
                for(int tx=0; tx < TILE_WIDTH; tx++, i++) {
                    p.drawImage(x*TILEE_WIDTH + tx*PCG_WIDTH, y*TILEE_HEIGHT + ty*PCG_HEIGHT, img[i]);
                }
            }

            int xb = x * TILEE_WIDTH;
            int yb = y * TILEE_HEIGHT;
            if(m_sel == y*COLS+x) {
                p.fillRect(xb,yb, TILE_WIDTH,TILE_HEIGHT, QColor(OVERLAY_SEL_COLOR_R,OVERLAY_SEL_COLOR_G,OVERLAY_SEL_COLOR_B, OVERLAY_ALPHA));
            }

            p.setPen(QColor(OVERLAY_FLAG_COLOR_R,OVERLAY_FLAG_COLOR_G,OVERLAY_FLAG_COLOR_B, OVERLAY_ALPHA));
            int flag = g_map.m_tiles[y*ROWS + x].m_flags[0];
#if CUR_GAMEDEF == GAMEDEF_PIYO
            for(int z=1; z <= TILE_FHEIGHT/4; z++) {
                if(flag & (1<<1)) // Fall through left not allowed
                    p.drawLine(xb+z,yb+z, xb+z,yb+TILE_FHEIGHT-z);
                if(flag & (1<<3)) // Fall through right not allowed
                    p.drawLine(xb+TILE_FWIDTH-z,yb+z, xb+TILE_FWIDTH-z,yb+TILE_FHEIGHT-z);
            }
            for(int z=1; z <= TILE_FWIDTH/4; z++) {
                if(flag & (1<<0)) // Fall through top not allowed
                    p.drawLine(xb+z,yb+z, xb+TILE_FWIDTH-z,yb+z);
                if(flag & (1<<2)) // Fall through bottom not allowed
                    p.drawLine(xb+z,yb+TILE_FHEIGHT-z, xb+TILE_FWIDTH-z,yb+TILE_FHEIGHT-z);
            }
#elif CUR_GAMEDEF == GAMEDEF_GBAGE
            if(flag & (1<<0)) { // Fall through not allowed
                for(int z=1; z <= TILE_FHEIGHT/4; z++) {
                    p.drawLine(xb+z,yb+z, xb+z,yb+TILE_FHEIGHT-z);
                    p.drawLine(xb+TILE_FWIDTH-z,yb+z, xb+TILE_FWIDTH-z,yb+TILE_FHEIGHT-z);
                }
                for(int z=1; z <= TILE_FWIDTH/4; z++) {
                    p.drawLine(xb+z,yb+z, xb+TILE_FWIDTH-z,yb+z);
                    p.drawLine(xb+z,yb+TILE_FHEIGHT-z, xb+TILE_FWIDTH-z,yb+TILE_FHEIGHT-z);
                }
            }
#endif
		}
	}
	p.end();
}

void Hokuto::TileEditor::mousePressEvent(QMouseEvent *event)
{
	if(event->buttons() & Qt::LeftButton) {
        int x = event->x() / TILEE_WIDTH;
        int y = event->y() / TILEE_WIDTH;
        if((x < COLS) && (x >= 0)) {
            if((y < ROWS) && (y >= 0)) {
                m_sel = y*ROWS + x;
				updateBox();
				repaint();
				return;
			}
		}
	}
	QWidget::mousePressEvent(event);
}
