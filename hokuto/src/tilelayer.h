#ifndef TILELAYER_H
#define TILELAYER_H

namespace Hokuto {
	struct Tile;
	struct TileLayer
	{
		TileLayer();

		void reset();
		void changeSize(int w, int h);
		void scroll(int x, int y);
		void setScroll(int x, int y);

		int m_xscroll, m_yscroll;
		int m_w, m_h;
		int m_xb, m_yb;
		Tile *m_tiles;
	};
}

#endif // TILELAYER_H
