//--- Qt includes
#include <QtGui>
//--- Editor includes
#include "util.h"
#include "screen.h"
#include "tile.h"
#include "tilelayer.h"

Hokuto::TileLayer::TileLayer()
{
	m_w = 128;
	m_h = 128;
	m_tiles = NULL;
	reset();
}

void Hokuto::TileLayer::reset()
{
	m_xscroll = 0;
	m_yscroll = 0;

	if(m_tiles) {
		free(m_tiles);
		m_tiles = NULL;
	}

	changeSize(m_w, m_h);
}

void Hokuto::TileLayer::changeSize(int w, int h)
{
	Tile *newtiles;

	newtiles = new Tile[w*h];
	if(m_tiles) {
		for(int y=0; y < h; y++) {
			for(int x=0; x < w; x++) {
				if(y < m_h && x < m_w) {
					newtiles[y*w + x] = m_tiles[y*m_w + x];
				}
			}
		}
		free(m_tiles);
	}

	m_tiles = newtiles;
	m_w = w;
	m_h = h;
    m_xb = m_w*PCG_WIDTH - g_screen.m_w;
    m_yb = m_h*PCG_HEIGHT - g_screen.m_h;

	scroll(0,0);
}

void Hokuto::TileLayer::scroll(int x, int y)
{
	setScroll(m_xscroll + x, m_yscroll + y);
}

void Hokuto::TileLayer::setScroll(int x, int y)
{
	m_xscroll = x;
	if(m_xscroll >= m_xb) m_xscroll = m_xb;
	if(m_xscroll <  0)    m_xscroll = 0;
	m_yscroll = y;
	if(m_yscroll >= m_yb) m_yscroll = m_yb;
	if(m_yscroll <  0)    m_yscroll = 0;
}
