//--- Qt includes
#include <QtGui>
#include <QApplication>
//--- Editor includes
#include "util.h"
#include "mapwindow.h"

Hokuto::MapWindow *Hokuto::g_mapwin;

int main(int argc, char **argv)
{
	QApplication app(argc, argv);

	// Setup "config" information
    app.setOrganizationName("DrillCity");
    app.setApplicationName("Hokuto");

	Hokuto::g_mapwin = new Hokuto::MapWindow;
	Hokuto::setUnmodified();
	Hokuto::g_mapwin->show();

	return app.exec();
}
