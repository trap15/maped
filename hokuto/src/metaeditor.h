#ifndef METAEDITOR_H
#define METAEDITOR_H

//--- Qt includes
#include <QtGui>
#include <QtWidgets>

namespace Hokuto {
	class MetaEditor : public QWidget
	{
		Q_OBJECT

	public:
		explicit MetaEditor(QWidget *parent = 0);
		void updateMeta();

	protected slots:
		void valueChange(int i);

	private:
        QGridLayout* m_grid;

		QLabel *m_lbl_w;
		QSpinBox *m_spn_w;
		QLabel *m_lbl_h;
		QSpinBox *m_spn_h;

		QLabel *m_lbl_pos;
		QLabel *m_lbl_pos2;

		QLabel *m_lbl_scr;
		QLabel *m_lbl_scr2;

		bool m_nochg; // hack
	};

	extern MetaEditor *g_meted;
}

#endif // METAEDITOR_H
