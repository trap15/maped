//--- Qt includes
#include <QtGui>
//--- Editor includes
#include "util.h"
#include "screen.h"
#include "paleditor.h"
#include "mapeditor.h"
#include "pcgviewer.h"
#include "tileeditor.h"

Hokuto::PalEditor *Hokuto::g_paled;

#define PAL_BLK_SIZE (8)
#define PAL_BLK_FULL (PAL_BLK_SIZE+2)

Hokuto::PalEditor::PalEditor(QWidget *parent) :
	QWidget(parent)
{
    m_sel = 0;
    m_nochg = false;

    QWidget *pad = new QWidget();
    pad->resize(PAL_COUNT * PAL_BLK_FULL, PAL_SIZE * PAL_BLK_FULL);
    pad->setMinimumSize(PAL_COUNT * PAL_BLK_FULL, PAL_SIZE * PAL_BLK_FULL);
    pad->setMaximumSize(PAL_COUNT * PAL_BLK_FULL, PAL_SIZE * PAL_BLK_FULL);
    QWidget *pad2 = new QWidget();
	QWidget *pad3 = new QWidget();

#define SETUP_SPIN(a,b,m) \
    m_lbl_##a = new QLabel(tr(b ":")); m_spn_##a = new QSpinBox; m_spn_##a->setRange(0,m); m_lbl_##a->setBuddy(m_spn_##a); \
    connect(m_spn_##a, SIGNAL(valueChanged(int)), this, SLOT(valueChange(int)))
#define SETUP_LBL(a,b) \
    m_lbl_##a = new QLabel(tr(b))

    SETUP_SPIN(r, "R", PAL_R_MASK);
    SETUP_SPIN(g, "G", PAL_G_MASK);
    SETUP_SPIN(b, "B", PAL_B_MASK);

    m_grid = new QGridLayout;
	m_grid->addWidget(pad,      0, 0, 4,1);
	m_grid->addWidget(pad2,     0, 1);
	m_grid->addWidget(m_lbl_r,  1, 1);
	m_grid->addWidget(m_spn_r,  1, 2);
	m_grid->addWidget(m_lbl_g,  2, 1);
	m_grid->addWidget(m_spn_g,  2, 2);
	m_grid->addWidget(m_lbl_b,  3, 1);
	m_grid->addWidget(m_spn_b,  3, 2);
	m_grid->addWidget(pad3,     8, 1);

    setLayout(m_grid);

	updatePal();
}

void Hokuto::PalEditor::paintEvent(QPaintEvent *event)
{
	(void)event;
	QPainter p(this);

    for(int v=0; v < PAL_COUNT; v++) {
        for(int i=0; i < PAL_SIZE; i++) {
            p.setPen(g_screen.m_pal[v*PAL_SIZE + i].getFullColor());

            p.fillRect(v*PAL_BLK_FULL,i*PAL_BLK_FULL, PAL_BLK_SIZE,PAL_BLK_SIZE,
                                g_screen.m_pal[v*PAL_SIZE + i].getFullColor());

            if(m_sel == v*PAL_SIZE+i) {
                p.fillRect(v*PAL_BLK_FULL,i*PAL_BLK_FULL, PAL_BLK_SIZE,PAL_BLK_SIZE,
                                QColor(0xFF, 0xFF, 0xFF, OVERLAY_ALPHA*2));
            }
        }
    }
    int end = PAL_COUNT*PAL_BLK_FULL;
    p.fillRect(end+0,0, width()-end-1,18,   qRgb(0,0,0));
    p.fillRect(end+1,1, width()-end-3,18-2, g_screen.m_pal[m_sel].getFullColor());
	p.end();
}

void Hokuto::PalEditor::mousePressEvent(QMouseEvent *event)
{
    if(event->buttons() & Qt::LeftButton) {
        int x = event->x() / PAL_BLK_FULL;
        int y = event->y() / PAL_BLK_FULL;
        if((x < PAL_COUNT) && (x >= 0)) {
            if((y < PAL_SIZE) && (y >= 0)) {
                m_sel = x*PAL_SIZE + y;
                updatePal();
				repaint();
				return;
            }
        }
    }
    QWidget::mousePressEvent(event);
}

void Hokuto::PalEditor::valueChange(int i)
{
    (void)i;
	if(!m_nochg) {
        g_screen.m_pal[m_sel] = (m_spn_b->value() << PAL_B_SHIFT) |
                                (m_spn_g->value() << PAL_G_SHIFT) |
                                (m_spn_r->value() << PAL_R_SHIFT);
		g_maped->repaint();
		g_pcgvw->repaint();
		g_tiled->repaint();
		Hokuto::setModified();
	}

    repaint();
}

void Hokuto::PalEditor::updatePal()
{
    m_nochg = true;
    m_spn_r->setValue((g_screen.m_pal[m_sel] >> PAL_R_SHIFT) & PAL_R_MASK);
    m_spn_g->setValue((g_screen.m_pal[m_sel] >> PAL_G_SHIFT) & PAL_G_MASK);
    m_spn_b->setValue((g_screen.m_pal[m_sel] >> PAL_B_SHIFT) & PAL_B_MASK);
    m_nochg = false;
}
