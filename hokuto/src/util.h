#ifndef UTIL_H
#define UTIL_H

//--- System includes
#include <stdint.h>

#include "gamedef.h"

///////////////////
// Configuration //
///////////////////

// Change to 1 if using on a big-endian machine
#define BIGENDIAN 0

#define OVERLAY_ALPHA 0x40
#define OVERLAY_LINE_COLOR_R 0xFF
#define OVERLAY_LINE_COLOR_G 0xFF
#define OVERLAY_LINE_COLOR_B 0xFF
#define OVERLAY_FLAG_COLOR_R 0xFF
#define OVERLAY_FLAG_COLOR_G 0x80
#define OVERLAY_FLAG_COLOR_B 0x80
#define OVERLAY_SEL_COLOR_R 0xFF
#define OVERLAY_SEL_COLOR_G 0xFF
#define OVERLAY_SEL_COLOR_B 0xFF

////////////////////////////
// Don't change past here //
////////////////////////////

namespace Hokuto {
	void setModified();
	void setUnmodified();
}

#define ES32(x) (\
	(((x) & 0xFF000000) >> 24) | \
	(((x) & 0x00FF0000) >>  8) | \
	(((x) & 0x0000FF00) <<  8) | \
	(((x) & 0x000000FF) << 24))

#define ES16(x) (\
	(((x) & 0xFF00) >> 8) | \
	(((x) & 0x00FF) << 8))

#if BIGENDIAN
# define BE32
# define BE16
# define LE32 ES32
# define LE16 ES16
#else
# define BE32 ES32
# define BE16 ES16
# define LE32
# define LE16
#endif

static uint8_t read8(void* buf, int* off) {
    uint8_t data = *(uint8_t*)((uint8_t*)buf + *off);
    *off += 1;
    return data;
}
static uint16_t read16(void* buf, int* off) {
    uint16_t data = *(uint16_t*)((uint8_t*)buf + *off);
    *off += 2;
    return BE16(data);
}
static uint32_t read32(void* buf, int* off) {
    uint32_t data = *(uint32_t*)((uint8_t*)buf + *off);
    *off += 4;
    return BE32(data);
}
static uint8_t  read8 (void* buf, int off) { return read8 (buf, &off); }
static uint16_t read16(void* buf, int off) { return read16(buf, &off); }
static uint32_t read32(void* buf, int off) { return read32(buf, &off); }

#endif // UTIL_H
