#ifndef GAMEDEF_H
#define GAMEDEF_H

#define GAMEDEF_PIYO  0
#define GAMEDEF_GBA4  1
#define GAMEDEF_GBA8  2
#define GAMEDEF_GBAGE 3

#define CUR_GAMEDEF GAMEDEF_GBAGE

#if CUR_GAMEDEF == GAMEDEF_PIYO
#  define GAME_STR "Piyo"

#  define MAX_MAP_WIDTH  (128)
#  define MAX_MAP_HEIGHT (128)
#  define MAP_SCROLL_SPD (8)

#  define PCG_BPP    (4)
#  define PCG_WIDTH  (8)
#  define PCG_HEIGHT (8)
#  define PCG_PLANAR (true)
#  define PCG_LEFT_MSB (true)
#  define PCG_UNIT_SIZE (8)

#  define TILE_WIDTH     (2)
#  define TILE_HEIGHT    (2)
#  define TILE_ATTR_SIZE (1)
#  define TILE_MINPAL    (0)
#  define TILE_MAXPAL    (PAL_COUNT-1)

#  define PAL_SIZE  (16)
#  define PAL_COUNT (2)
#  define PAL_DEPTH (8)
#  define PAL_R_BITS (2)
#  define PAL_G_BITS (2)
#  define PAL_B_BITS (2)
#  define PAL_R_SHIFT (0)
#  define PAL_G_SHIFT (2)
#  define PAL_B_SHIFT (4)
#elif CUR_GAMEDEF == GAMEDEF_GBA4
#  define GAME_STR "GBA 4bpp"

#  define MAX_MAP_WIDTH  (128)
#  define MAX_MAP_HEIGHT (128)
#  define MAP_SCROLL_SPD (8)

#  define PCG_BPP    (4)
#  define PCG_WIDTH  (8)
#  define PCG_HEIGHT (8)
#  define PCG_PLANAR (false)
#  define PCG_LEFT_MSB (false)
#  define PCG_UNIT_SIZE (8)

#  define TILE_WIDTH     (2)
#  define TILE_HEIGHT    (2)
#  define TILE_ATTR_SIZE (1)
#  define TILE_MINPAL    (0)
#  define TILE_MAXPAL    (PAL_COUNT-1)

#  define PAL_SIZE  (16)
#  define PAL_COUNT (16)
#  define PAL_DEPTH (16)
#  define PAL_R_BITS (5)
#  define PAL_G_BITS (5)
#  define PAL_B_BITS (5)
#  define PAL_R_SHIFT (0)
#  define PAL_G_SHIFT (5)
#  define PAL_B_SHIFT (10)
#elif CUR_GAMEDEF == GAMEDEF_GBA8
#  define GAME_STR "GBA 8bpp"

#  define MAX_MAP_WIDTH  (128)
#  define MAX_MAP_HEIGHT (128)
#  define MAP_SCROLL_SPD (8)

#  define PCG_BPP    (8)
#  define PCG_WIDTH  (8)
#  define PCG_HEIGHT (8)
#  define PCG_PLANAR (false)
#  define PCG_LEFT_MSB (false)
#  define PCG_UNIT_SIZE (8)

#  define TILE_WIDTH     (2)
#  define TILE_HEIGHT    (2)
#  define TILE_ATTR_SIZE (1)
#  define TILE_MINPAL    (0)
#  define TILE_MAXPAL    (PAL_COUNT-1)

#  define PAL_SIZE  (256)
#  define PAL_COUNT (1)
#  define PAL_DEPTH (16)
#  define PAL_R_BITS (5)
#  define PAL_G_BITS (5)
#  define PAL_B_BITS (5)
#  define PAL_R_SHIFT (0)
#  define PAL_G_SHIFT (5)
#  define PAL_B_SHIFT (10)
#elif CUR_GAMEDEF == GAMEDEF_GBAGE
#  define GAME_STR "GBAge"

#  define MAX_MAP_WIDTH  (128)
#  define MAX_MAP_HEIGHT (128)
#  define MAP_SCROLL_SPD (8)

#  define PCG_BPP    (4)
#  define PCG_WIDTH  (8)
#  define PCG_HEIGHT (8)
#  define PCG_PLANAR (false)
#  define PCG_LEFT_MSB (false)
#  define PCG_UNIT_SIZE (8)

#  define TILE_WIDTH     (2)
#  define TILE_HEIGHT    (2)
#  define TILE_ATTR_SIZE (2)
#  define TILE_MINPAL    (0)
#  define TILE_MAXPAL    (PAL_COUNT-1)

#  define PAL_SIZE  (16)
#  define PAL_COUNT (16)
#  define PAL_DEPTH (16)
#  define PAL_R_BITS (5)
#  define PAL_G_BITS (5)
#  define PAL_B_BITS (5)
#  define PAL_R_SHIFT (0)
#  define PAL_G_SHIFT (5)
#  define PAL_B_SHIFT (10)
#endif

#define PCG_PITCH (PCG_WIDTH * PCG_BPP / PCG_UNIT_SIZE)
#define PCG_PIXPERUNIT (PCG_UNIT_SIZE / PCG_BPP)
#define PCG_MASK ((1 << PCG_BPP)-1)

#define TILE_SIZE    (TILE_WIDTH * TILE_HEIGHT)
#define TILE_FWIDTH  (PCG_WIDTH * TILE_WIDTH)
#define TILE_FHEIGHT (PCG_HEIGHT * TILE_HEIGHT)

#define PAL_R_MASK ((1 << PAL_R_BITS)-1)
#define PAL_G_MASK ((1 << PAL_G_BITS)-1)
#define PAL_B_MASK ((1 << PAL_B_BITS)-1)

#endif // GAMEDEF_H
