#ifndef MAPWINDOW_H
#define MAPWINDOW_H

//--- Qt includes
#include <QMainWindow>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFrame>
#include <QLabel>
#include <QScrollBar>

namespace Hokuto {
    class MapEditor;
    class PalEditor;
    class PCGViewer;
    class TileEditor;
    class MetaEditor;
	class MapWindow : public QMainWindow
	{
		Q_OBJECT
	public:
		explicit MapWindow(QWidget *parent = 0);

		QScrollBar *m_scrollx;
		QScrollBar *m_scrolly;

	protected:
		void closeEvent(QCloseEvent *event);

	private slots:
		void open();
		void save();
		void saveAs();
		void closeFile();

		void loadPCG();
		void savePCG();
		void loadPAL();
		void savePAL();

	private:
		void createActions();
		void createMenus();

		void readSettings();
		void writeSettings();

		QGridLayout* m_grid;

		QVBoxLayout* m_vlyt1;
		QVBoxLayout* m_vlyt2;
		QHBoxLayout* m_hlyt1;
		QHBoxLayout* m_hlyt2;
        QFrame *m_line1;
        QFrame *m_line2;
        QFrame *m_line3;
        QFrame *m_line4;
        QLabel *m_label;

		QMenu* m_fileMenu;
		QAction* m_openAct;
		QAction* m_saveAct;
		QAction* m_saveAsAct;
		QAction* m_closeAct;
		QAction* m_exitAct;

		QMenu* m_rsrcMenu;
		QMenu* m_pcgMenu;
		QAction* m_loadPCGAct;
		QAction* m_savePCGAct;
		QMenu* m_palMenu;
		QAction* m_loadPALAct;
		QAction* m_savePALAct;

		QMenuBar* m_menuBar;
		Hokuto::MapEditor* m_maped;
        Hokuto::PalEditor* m_paled;
		Hokuto::PCGViewer* m_pcgvw;
		Hokuto::TileEditor* m_tiled;
		Hokuto::MetaEditor* m_meted;

		QString m_curfile;
	};

	extern bool g_modified;
	extern MapWindow *g_mapwin;
}

#endif // MAPWINDOW_H
