#ifndef SCREEN_H
#define SCREEN_H

namespace Hokuto {
#if PAL_DEPTH == 8
    typedef uint8_t Pixel;
#elif PAL_DEPTH == 16
    typedef uint16_t Pixel;
#elif PAL_DEPTH == 32
    typedef uint32_t Pixel;
#else
    typedef uint32_t Pixel;
#endif
    struct Color
	{
		Color() : r(0), g(0), b(0) {}

        Color& operator=(Pixel col) {
            r = (col >> PAL_R_SHIFT) & PAL_R_MASK;
            g = (col >> PAL_G_SHIFT) & PAL_G_MASK;
            b = (col >> PAL_B_SHIFT) & PAL_B_MASK;
			return *this;
		}

        operator Pixel() {
            return (r << PAL_R_SHIFT) | (g << PAL_G_SHIFT) | (b << PAL_B_SHIFT);
		}

		uint32_t getFullColor() {
            return qRgb(r << (8-PAL_R_BITS), g << (8-PAL_G_BITS), b << (8-PAL_B_BITS));
		}

		uint8_t r,g,b;
	};
	struct PCG
	{
		PCG();

        Pixel m_pix[PCG_WIDTH][PCG_HEIGHT];
	};

	struct TileLayer;
	struct Screen
	{
		Screen();

		void reset();

		void update();
		QImage *getImage();
		void scroll(int x, int y);
		void setScroll(int x, int y);

		bool loadPCG(QString path);
		bool loadPCG(void *data);
		bool savePCG(QString path);
		bool savePCG(uchar* &data, qint64 &len);

		bool loadPAL(QString path);
		bool loadPAL(void *data);
		bool savePAL(QString path);
		bool savePAL(uchar* &data, qint64 &len);

		int m_w, m_h;
		TileLayer *m_bg0;
        Color m_pal[PAL_COUNT*PAL_SIZE]; // Palettes * Colors
		PCG m_pcg[512];

		void renderPCG(int code, int pal, QImage &img, int xo=0, int yo=0, int xs=0, int ys=0);

	private:
		void _draw_bg(TileLayer *bg);

		QImage m_img;
	};

	extern Screen g_screen;
}

#endif // SCREEN_H
