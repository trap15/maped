#ifndef MAPEDITOR_H
#define MAPEDITOR_H

//--- Qt includes
#include <QtGui>
#include <QtWidgets>

enum {
	KEY_UP    = 1<<0,
	KEY_DOWN  = 1<<1,
	KEY_LEFT  = 1<<2,
	KEY_RIGHT = 1<<3,
	KEY_DIR   = 0xF
};

namespace Hokuto {
	class MapEditor : public QWidget
	{
		Q_OBJECT

	public:
		explicit MapEditor(QWidget *parent = 0);

		Qt::MouseButtons m_mousebtn;
		int m_mousex, m_mousey;

		void doScroll(int x, int y);
		void setScroll(int x, int y);

	protected:
		void paintEvent(QPaintEvent *event);
		void keyPressEvent(QKeyEvent *event);
		void keyReleaseEvent(QKeyEvent *event);
		void mouseMoveEvent(QMouseEvent *event);
		void mousePressEvent(QMouseEvent *event);
		void mouseReleaseEvent(QMouseEvent *event);
		void timerEvent(QTimerEvent *event);

	private slots:
		void setScrollX(int);
		void setScrollY(int);

	private:
		QBasicTimer m_timer;
		unsigned int m_rawkeys;
		unsigned int m_keys;

		void placeTile(int x, int y, int tile);
		void mainUpdate();
		void doMouse();
	};

    extern MapEditor *g_maped;
}

#endif // MAPEDITOR_H
