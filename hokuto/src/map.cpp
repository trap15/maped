//--- Qt includes
#include <QtGui>
//--- Editor includes
#include "util.h"
#include "tile.h"
#include "map.h"
#include "tilelayer.h"
#include "screen.h"
#include "mapwindow.h"

Hokuto::Map Hokuto::g_map;

Hokuto::MapTile::MapTile()
{
    for(int i = 0; i < TILE_ATTR_SIZE; i++) {
        m_flags[i] = 0x00;
    }
    for(int i = 0; i < TILE_SIZE; i++) {
        m_tile[i] = Tile();
    }
}

Hokuto::Map::Map()
{
	m_w = 64;
	m_h = 64;
	m_map = NULL;
	reset();
}

void Hokuto::Map::changeSize(int w, int h)
{
	uint8_t *newmap;
	newmap = (uint8_t*)malloc(w * h);
	for(int y = 0; y < h; y++) {
		for(int x = 0; x < w; x++) {
			if(y < m_h && x < m_w && m_map) {
				newmap[y*w + x] = m_map[y*m_w + x];
			}else{
				newmap[y*w + x] = 0;
			}
		}
	}

	if(m_map) {
		free(m_map);
		m_map = NULL;
	}
	m_w = w;
	m_h = h;
    m_xb = m_w*TILE_FWIDTH - g_screen.m_w;
    m_yb = m_h*TILE_FHEIGHT - g_screen.m_h;
	m_map = newmap;

    m_img = QImage(m_w*TILE_FWIDTH, m_h*TILE_FHEIGHT, QImage::Format_ARGB32);

    if(g_screen.m_bg0) g_screen.m_bg0->changeSize(m_w*TILE_WIDTH, m_h*TILE_HEIGHT);

	scroll(0,0);

	if(g_mapwin) {
		if(g_mapwin->m_scrollx) g_mapwin->m_scrollx->setMaximum(m_xb);
		if(g_mapwin->m_scrolly) g_mapwin->m_scrolly->setMaximum(m_yb);
	}
}

void Hokuto::Map::scroll(int x, int y)
{
	setScroll(m_xscroll + x, m_yscroll + y);
}

void Hokuto::Map::setScroll(int x, int y)
{
	m_xscroll = x;
	if(m_xscroll >= m_xb) m_xscroll = m_xb;
	if(m_xscroll <  0)    m_xscroll = 0;
	m_yscroll = y;
	if(m_yscroll >= m_yb) m_yscroll = m_yb;
	if(m_yscroll <  0)    m_yscroll = 0;
}

void Hokuto::Map::reset()
{
	if(m_map) {
		free(m_map);
		m_map = NULL;
	}

	for(int i = 0; i < 256; i++) {
		m_tiles[i] = MapTile();
	}

	m_xscroll = 0;
	m_yscroll = 0;

	changeSize(m_w, m_h);
}

void Hokuto::Map::_draw_flags()
{
	QPainter p;
	int flag;

    if(!TILE_ATTR_SIZE)
        return;

    p.begin(&m_img);
    p.setPen(QColor(OVERLAY_FLAG_COLOR_R,OVERLAY_FLAG_COLOR_G,OVERLAY_FLAG_COLOR_B, OVERLAY_ALPHA));
	for(int y=0; y < m_h; y++) {
		for(int x=0; x < m_w; x++) {
            flag = m_tiles[m_map[y*m_w + x]].m_flags[0];
            int yb = (y*TILE_FHEIGHT) - m_yscroll;
            int xb = (x*TILE_FWIDTH) - m_xscroll;

#if CUR_GAMEDEF == GAMEDEF_PIYO
            for(int z=1; z <= TILE_FHEIGHT/4; z++) {
                if(flag & (1<<1)) // Fall through left not allowed
                    p.drawLine(xb+z,yb+z, xb+z,yb+TILE_FHEIGHT-z);
                if(flag & (1<<3)) // Fall through right not allowed
                    p.drawLine(xb+TILE_FWIDTH-z,yb+z, xb+TILE_FWIDTH-z,yb+TILE_FHEIGHT-z);
            }
            for(int z=1; z <= TILE_FWIDTH/4; z++) {
                if(flag & (1<<0)) // Fall through top not allowed
                    p.drawLine(xb+z,yb+z, xb+TILE_FWIDTH-z,yb+z);
                if(flag & (1<<2)) // Fall through bottom not allowed
                    p.drawLine(xb+z,yb+TILE_FHEIGHT-z, xb+TILE_FWIDTH-z,yb+TILE_FHEIGHT-z);
            }
#elif CUR_GAMEDEF == GAMEDEF_GBAGE
            if(flag & (1<<0)) { // Fall through not allowed
                for(int z=1; z <= TILE_FHEIGHT/4; z++) {
                    p.drawLine(xb+z,yb+z, xb+z,yb+TILE_FHEIGHT-z);
                    p.drawLine(xb+TILE_FWIDTH-z,yb+z, xb+TILE_FWIDTH-z,yb+TILE_FHEIGHT-z);
                }
                for(int z=1; z <= TILE_FWIDTH/4; z++) {
                    p.drawLine(xb+z,yb+z, xb+TILE_FWIDTH-z,yb+z);
                    p.drawLine(xb+z,yb+TILE_FHEIGHT-z, xb+TILE_FWIDTH-z,yb+TILE_FHEIGHT-z);
                }
            }
#endif
		}
	}

	p.end();
}

void Hokuto::Map::_draw_grid()
{
	QPainter p;

	p.begin(&m_img);
    p.setPen(QColor(OVERLAY_LINE_COLOR_R,OVERLAY_LINE_COLOR_G,OVERLAY_LINE_COLOR_B, OVERLAY_ALPHA));
	for(int x=0; x < m_w; x++) {
        int xb = x*TILE_FWIDTH - m_xscroll;
        p.drawLine(xb, 0, xb, m_h*TILE_FHEIGHT);
	}

	for(int y=0; y < m_h; y++) {
        int yb = y*TILE_FHEIGHT - m_yscroll;
        p.drawLine(0, yb, m_w*TILE_FWIDTH, yb);
	}
	p.end();
}

void Hokuto::Map::_update_screen()
{
	for(int y=0; y < m_h; y++) {
		for(int x=0; x < m_w; x++) {
            int i = 0;
            for(int ty=0; ty < TILE_HEIGHT; ty++) {
                for(int tx=0; tx < TILE_WIDTH; tx++, i++) {
                    g_screen.m_bg0->m_tiles[(y*TILE_HEIGHT+ty)*g_screen.m_bg0->m_w + (x*TILE_WIDTH+tx)] = m_tiles[m_map[y*m_w + x]].m_tile[i];
                }
            }
		}
	}
}

bool Hokuto::Map::loadMAP(QString path)
{
	QFile file(path);
	if(!file.open(QIODevice::ReadOnly)) {
		return false;
	}
	uchar *data = file.map(0, file.size());
	file.close();
	return loadMAP(data);
}

bool Hokuto::Map::loadMAP(void *data)
{
	int o, tiles, pcg;
    o = 0;
    if(read32(data, &o) != 0x484D4150) { // Not a valid MAP
        return false;
    }

	reset();
	g_screen.reset();

    tiles = read8(data, &o)+1;
    pcg = read16(data, &o)+1;
    m_w = read8(data, &o)+1;
    m_h = read8(data, &o)+1;
	reset();

	for(int i=0; i < tiles; i++) {
        for(int t=0; t < TILE_ATTR_SIZE; t++)
            m_tiles[i].m_flags[t] = read8(data, &o);
        for(int t=0; t < TILE_SIZE; t++) {
            m_tiles[i].m_tile[t].m_code = read8(data, &o);
        }
	}
    for(int y=0; y < m_h; y++) {
		for(int x=0; x < m_w; x++) {
            m_map[y*m_w + x] = read8(data, &o);
		}
	}
    for(int i=0; i < pcg; i++) {
        for(int y=0; y < PCG_HEIGHT; y++) {
            for(int x=0; x < PCG_WIDTH; x++) {
				g_screen.m_pcg[i].m_pix[x][y] = 0;
			}
            if(PCG_PLANAR) {
                for(int b=0; b < PCG_BPP; b++) {
                    int px = read8(data, &o);
                    for(int x=0; x < PCG_WIDTH; x++) {
                        if(PCG_LEFT_MSB)
                            g_screen.m_pcg[i].m_pix[x][y] |= ((px >> (PCG_WIDTH-x-1)) & 1) << b;
                        else
                            g_screen.m_pcg[i].m_pix[x][y] |= ((px >> x) & 1) << b;
                    }
                }
            }else{
                for(int x=0; x < PCG_PITCH; x++) {
                    int px = read8(data, &o);
                    for(int b=0; b < PCG_PIXPERUNIT; b++) {
                        if(PCG_LEFT_MSB)
                            g_screen.m_pcg[i].m_pix[x*PCG_PIXPERUNIT+b][y] = (px >> (PCG_UNIT_SIZE-((b+1)*PCG_BPP))) & PCG_MASK;
                        else
                            g_screen.m_pcg[i].m_pix[x*PCG_PIXPERUNIT+b][y] = (px >> (b*PCG_BPP)) & PCG_MASK;
                    }
                }
            }
		}
	}
    for(int p=0; p < PAL_COUNT; p++) {
        for(int i=0; i < PAL_SIZE; i++) {
            switch(PAL_DEPTH) {
                case 8:
                    g_screen.m_pal[p*PAL_SIZE + i] = read8(data, &o);
                    break;
                case 16:
                    g_screen.m_pal[p*PAL_SIZE + i] = read16(data, &o);
                    break;
                case 32:
                    g_screen.m_pal[p*PAL_SIZE + i] = read32(data, &o);
                    break;
            }
        }
    }

	return true;
}

bool Hokuto::Map::saveMAP(QString path)
{
	uchar *data;
	qint64 len;
	QFile file(path);
	if(!file.open(QIODevice::WriteOnly)) {
		return false;
	}
	saveMAP(data, len);
	return file.write((const char*)data, len) == len;
}

bool Hokuto::Map::saveMAP(uchar* &data, qint64 &len)
{
	QByteArray buf;
	int tiles, pcg;

	tiles = 0;
	pcg = 0;

	// Optimization time!
	for(int y=0; y < m_h; y++) {
		for(int x=0; x < m_w; x++) {
			if(m_map[y*m_w + x] > tiles)
				tiles = m_map[y*m_w + x];
		}
	}

	for(int i=0; i <= tiles; i++) {
        for(int t=0; t < TILE_SIZE; t++) {
            if(m_tiles[i].m_tile[t].m_code > pcg)
                pcg = m_tiles[i].m_tile[t].m_code;
        }
	}

    buf.append("HMAP");

	buf.append(tiles & 0xFF); // Tiles
	buf.append(pcg >> 8); // PCG count high
	buf.append(pcg & 0xFF); // PCG count low
	buf.append((m_w-1) & 0xFF); // Width
	buf.append((m_h-1) & 0xFF); // Height

    for(int i=0; i <= tiles; i++) {
        for(int t=0; t < TILE_ATTR_SIZE; t++)
            buf.append(m_tiles[i].m_flags[t]);
        for(int t=0; t < TILE_SIZE; t++) {
            buf.append(m_tiles[i].m_tile[t].m_code);
        }
	}
    for(int y=0; y < m_h; y++) {
		for(int x=0; x < m_w; x++) {
			buf.append(m_map[y*m_w + x]);
		}
	}
    for(int i=0; i <= pcg; i++) {
        for(int y=0; y < PCG_HEIGHT; y++) {
            if(PCG_PLANAR) {
                for(int b=0; b < PCG_BPP; b++) {
                    uint8_t v = 0;
                    for(int x=0; x < PCG_WIDTH; x++) {
                        if(PCG_LEFT_MSB)
                            v |= ((g_screen.m_pcg[i].m_pix[x][y] >> b) & 1) << (PCG_WIDTH-x-1);
                        else
                            v |= ((g_screen.m_pcg[i].m_pix[x][y] >> b) & 1) << x;
                    }
                    buf.append(v);
                }
            }else{
                for(int x=0; x < PCG_PITCH; x++) {
                    uint8_t v = 0;
                    for(int b=0; b < PCG_PIXPERUNIT; b++) {
                        if(PCG_LEFT_MSB) {
                            v <<= PCG_BPP;
                            v |= g_screen.m_pcg[i].m_pix[x*PCG_PIXPERUNIT+b][y];
                        }else{
                            v >>= PCG_BPP;
                            v |= g_screen.m_pcg[i].m_pix[x*PCG_PIXPERUNIT+b][y] << PCG_BPP;
                        }
                    }
                    buf.append(v);
                }
            }
		}
	}
    for(int p=0; p < PAL_COUNT; p++) {
        for(int i=0; i < PAL_SIZE; i++) {
            switch(PAL_DEPTH) {
                case 8:
                    buf.append(g_screen.m_pal[p*PAL_SIZE + i]);
                    break;
                case 16:
                    buf.append(g_screen.m_pal[p*PAL_SIZE + i] >> 8);
                    buf.append(g_screen.m_pal[p*PAL_SIZE + i] >> 0);
                    break;
                case 32:
                    buf.append(g_screen.m_pal[p*PAL_SIZE + i] >> 24);
                    buf.append(g_screen.m_pal[p*PAL_SIZE + i] >> 16);
                    buf.append(g_screen.m_pal[p*PAL_SIZE + i] >> 8);
                    buf.append(g_screen.m_pal[p*PAL_SIZE + i] >> 0);
                    break;
            }
		}
	}

	len = buf.size();
	data = new uchar[len];
	memcpy(data, buf.data(), len);
	return true;
}

void Hokuto::Map::update()
{
	// Clear the overlay first
	m_img.fill(qRgba(0, 0, 0, 0));

	// Draw the overlay stuff
	_draw_flags();
	_draw_grid();

	// Update the main screen stuff
	_update_screen();
}

QImage *Hokuto::Map::getImage()
{
	update();
	return &m_img;
}
