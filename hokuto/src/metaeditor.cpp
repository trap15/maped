//--- Qt includes
#include <QtGui>
//--- Editor includes
#include "util.h"
#include "screen.h"
#include "metaeditor.h"
#include "mapeditor.h"
#include "tile.h"
#include "map.h"

Hokuto::MetaEditor *Hokuto::g_meted;

Hokuto::MetaEditor::MetaEditor(QWidget *parent) :
	QWidget(parent)
{

	QWidget *pad = new QWidget;

#define SETUP_SPIN(a,b,m) \
    m_lbl_##a = new QLabel(tr(b ":")); m_spn_##a = new QSpinBox; m_spn_##a->setRange(1,m); m_lbl_##a->setBuddy(m_spn_##a); \
	connect(m_spn_##a, SIGNAL(valueChanged(int)), this, SLOT(valueChange(int)))
#define SETUP_LBL(a,b) \
    m_lbl_##a = new QLabel(tr(b))

    SETUP_SPIN(w, "Width", MAX_MAP_WIDTH);
    SETUP_SPIN(h, "Height", MAX_MAP_HEIGHT);
	SETUP_LBL(pos, "Position:");
	SETUP_LBL(pos2, "");
	SETUP_LBL(scr, "Scroll:");
	SETUP_LBL(scr2, "");

    m_grid = new QGridLayout;
	m_grid->addWidget(m_lbl_w,    0, 0);
	m_grid->addWidget(m_spn_w,    0, 1);
	m_grid->addWidget(m_lbl_h,    1, 0);
	m_grid->addWidget(m_spn_h,    1, 1);
	m_grid->addWidget(m_lbl_pos,  2, 0);
	m_grid->addWidget(m_lbl_pos2, 2, 1);
	m_grid->addWidget(m_lbl_scr,  3, 0);
	m_grid->addWidget(m_lbl_scr2, 3, 1);
	m_grid->addWidget(pad,        5, 5);

    setLayout(m_grid);

	updateMeta();
}

void Hokuto::MetaEditor::valueChange(int i)
{
	(void)i;
	if(!m_nochg) {
		g_map.changeSize(m_spn_w->value(), m_spn_h->value());
	}
}

void Hokuto::MetaEditor::updateMeta()
{
	static int mousex=0, mousey=0;

	if(g_maped) {
		int x = g_maped->mapFromGlobal(QCursor::pos()).x();
		int y = g_maped->mapFromGlobal(QCursor::pos()).y();
		if(x >= 0 && x < g_maped->size().width()) {
			if(y >= 0 && y < g_maped->size().height()) {
				mousex = x;
				mousey = y;
			}
		}
	}

	m_nochg = true;
	m_spn_w->setValue(g_map.m_w);
	m_spn_h->setValue(g_map.m_h);
	m_nochg = false;

	m_lbl_pos2->setText(QString("%0,%1").arg(mousex).arg(mousey));
	m_lbl_scr2->setText(QString("%0,%1").arg(g_map.m_xscroll).arg(g_map.m_yscroll));
}
