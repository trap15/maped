//--- Qt includes
#include <QtGui>
//--- Editor includes
#include "util.h"
#include "screen.h"
#include "tile.h"
#include "map.h"
#include "mapeditor.h"
#include "paleditor.h"
#include "metaeditor.h"
#include "pcgviewer.h"
#include "tileeditor.h"
#include "mapwindow.h"

#include "hokuto.h"

bool Hokuto::g_modified;

Hokuto::MapWindow::MapWindow(QWidget *parent) :
	QMainWindow(parent)
{
	QWidget *central = new QWidget(this);

	g_screen.reset();
	g_map.reset();

    m_line1 = new QFrame();
	m_line1->setFrameShape(QFrame::HLine);
    m_line1->setFrameShadow(QFrame::Sunken);

    m_line2 = new QFrame();
    m_line2->setFrameShape(QFrame::VLine);
    m_line2->setFrameShadow(QFrame::Sunken);

    m_line3 = new QFrame();
    m_line3->setFrameShape(QFrame::VLine);
    m_line3->setFrameShadow(QFrame::Sunken);

    m_line4 = new QFrame();
    m_line4->setFrameShape(QFrame::HLine);
    m_line4->setFrameShadow(QFrame::Sunken);

    m_menuBar = new QMenuBar();
    m_maped = new Hokuto::MapEditor(this);
    m_paled = new Hokuto::PalEditor();
	m_pcgvw = new Hokuto::PCGViewer();
	m_tiled = new Hokuto::TileEditor();
	m_meted = new Hokuto::MetaEditor();
	g_meted = m_meted;
	g_tiled = m_tiled;
    g_pcgvw = m_pcgvw;
    g_paled = m_paled;
    g_maped = m_maped;

	m_scrollx = new QScrollBar;
	m_scrollx->setOrientation(Qt::Horizontal);
	m_scrollx->setMinimum(0);
    m_scrollx->setMaximum(g_map.m_w*PCG_WIDTH);
	m_scrollx->setTracking(true);
    m_scrollx->setPageStep(PCG_WIDTH*4);
	connect(m_scrollx, SIGNAL(valueChanged(int)), m_maped, SLOT(setScrollX(int)));

	m_scrolly = new QScrollBar;
    m_scrolly->setOrientation(Qt::Vertical);
	m_scrolly->setMinimum(0);
    m_scrolly->setMaximum(g_map.m_h*PCG_HEIGHT);
	m_scrolly->setTracking(true);
    m_scrolly->setPageStep(PCG_HEIGHT*4);
	connect(m_scrolly, SIGNAL(valueChanged(int)), m_maped, SLOT(setScrollY(int)));

	m_grid = new QGridLayout;
	m_grid->addWidget(m_maped,   0, 0);
	m_grid->addWidget(m_scrollx, 1, 0);
	m_grid->addWidget(m_scrolly, 0, 1);

	m_vlyt1 = new QVBoxLayout;
	m_vlyt1->addWidget(m_pcgvw);
	m_vlyt1->addWidget(m_line1);
	m_vlyt1->addWidget(m_paled);

	m_vlyt2 = new QVBoxLayout;
	m_vlyt2->addWidget(m_tiled);
	m_vlyt2->addWidget(m_line4);
	m_vlyt2->addWidget(m_meted);

	m_hlyt1 = new QHBoxLayout(central);
	m_hlyt1->addLayout(m_grid);
	m_hlyt1->addWidget(m_line2);
	m_hlyt1->addLayout(m_vlyt2);
	m_hlyt1->addWidget(m_line3);
	m_hlyt1->addLayout(m_vlyt1);

	central->setLayout(m_hlyt1);
	setCentralWidget(central);

	createActions();
	createMenus();

	readSettings();

    setWindowTitle(QString("Untitled[*] - Hokuto Map Editor for %1").arg(GAME_STR));
	setUnifiedTitleAndToolBarOnMac(true);
	setMenuBar(m_menuBar);
	adjustSize();

	setFixedSize(size());

	m_curfile = "";
}

void Hokuto::MapWindow::closeEvent(QCloseEvent *ev)
{
	writeSettings();
	ev->accept();
	qApp->quit();
}

void Hokuto::MapWindow::open()
{
	QString filename = QFileDialog::getOpenFileName(this,
			tr("Open MAP File"), "",
			tr("MAP Data File (*.map);;All Files (*)"));

	if (filename.isEmpty())
		return;
	m_curfile = filename;
    setWindowTitle(QString("%1[*] - Hokuto Map Editor for %2").arg(m_curfile, GAME_STR));

	g_map.loadMAP(m_curfile);
	m_paled->updatePal();
	m_paled->repaint();
	m_pcgvw->repaint();
	m_tiled->repaint();
	m_tiled->updateBox();

	Hokuto::setUnmodified();
}

void Hokuto::MapWindow::save()
{
	if(m_curfile.isEmpty())
		saveAs();
	g_map.saveMAP(m_curfile);
	setUnmodified();
}

void Hokuto::MapWindow::saveAs()
{
	QString filename = QFileDialog::getSaveFileName(this,
			tr("Save MAP File"), "",
			tr("MAP Data File (*.map);;All Files (*)"));

	if (filename.isEmpty())
		return;
	m_curfile = filename;
	save();
}

void Hokuto::MapWindow::closeFile()
{
	m_curfile = "";
    setWindowTitle(QString("Untitled[*] - Hokuto Map Editor for %1").arg(GAME_STR));
	Hokuto::setModified();
}

void Hokuto::MapWindow::loadPCG()
{
	QString filename = QFileDialog::getOpenFileName(this,
			tr("Open PCG File"), "",
			tr("PCG Data File (*.pcg);;All Files (*)"));

	if (filename.isEmpty())
		return;
	setModified();
	g_screen.loadPCG(filename);
    m_pcgvw->repaint();
	m_tiled->repaint();
}

void Hokuto::MapWindow::savePCG()
{
	QString filename = QFileDialog::getSaveFileName(this,
			tr("Save PCG File"), "",
			tr("PCG Data File (*.pcg);;All Files (*)"));

	if (filename.isEmpty())
		return;
	g_screen.savePCG(filename);
}

void Hokuto::MapWindow::loadPAL()
{
	QString filename = QFileDialog::getOpenFileName(this,
			tr("Open PAL File"), "",
			tr("PAL Data File (*.pal);;All Files (*)"));

	if (filename.isEmpty())
		return;
	setModified();
	g_screen.loadPAL(filename);
    m_paled->updatePal();
    m_paled->repaint();
	m_tiled->repaint();
}

void Hokuto::MapWindow::savePAL()
{
	QString filename = QFileDialog::getSaveFileName(this,
			tr("Save PAL File"), "",
			tr("PAL Data File (*.pal);;All Files (*)"));

	if (filename.isEmpty())
		return;
	g_screen.savePAL(filename);
}

void Hokuto::MapWindow::createActions()
{
	m_openAct = new QAction(tr("&Open..."), this);
	m_openAct->setShortcuts(QKeySequence::Open);
	connect(m_openAct, SIGNAL(triggered()), this, SLOT(open()));

	m_saveAct = new QAction(tr("&Save..."), this);
	m_saveAct->setShortcuts(QKeySequence::Save);
	connect(m_saveAct, SIGNAL(triggered()), this, SLOT(save()));

	m_saveAsAct = new QAction(tr("Save &As..."), this);
	m_saveAsAct->setShortcuts(QKeySequence::SaveAs);
	connect(m_saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));

	m_closeAct = new QAction(tr("&Close..."), this);
	m_closeAct->setShortcuts(QKeySequence::Close);
	connect(m_closeAct, SIGNAL(triggered()), this, SLOT(closeFile()));

	m_exitAct = new QAction(tr("E&xit"), this);
	m_exitAct->setShortcuts(QKeySequence::Quit);
	connect(m_exitAct, SIGNAL(triggered()), this, SLOT(close()));

	m_loadPCGAct = new QAction(tr("Load PCG"), this);
	connect(m_loadPCGAct, SIGNAL(triggered()), this, SLOT(loadPCG()));

	m_savePCGAct = new QAction(tr("Save PCG"), this);
	connect(m_savePCGAct, SIGNAL(triggered()), this, SLOT(savePCG()));

	m_loadPALAct = new QAction(tr("Load PAL"), this);
	connect(m_loadPALAct, SIGNAL(triggered()), this, SLOT(loadPAL()));

	m_savePALAct = new QAction(tr("Save PAL"), this);
	connect(m_savePALAct, SIGNAL(triggered()), this, SLOT(savePAL()));
}

void Hokuto::MapWindow::createMenus()
{
	m_fileMenu = m_menuBar->addMenu(tr("&File"));
	m_fileMenu->addAction(m_openAct);
	m_fileMenu->addAction(m_saveAct);
	m_fileMenu->addAction(m_saveAsAct);
	m_fileMenu->addAction(m_closeAct);
	m_fileMenu->addSeparator();
	m_fileMenu->addAction(m_exitAct);

	m_rsrcMenu = m_menuBar->addMenu(tr("&Resources"));
	m_pcgMenu = m_rsrcMenu->addMenu(tr("PCG"));
	m_pcgMenu->addAction(m_loadPCGAct);
	m_pcgMenu->addAction(m_savePCGAct);
	m_palMenu = m_rsrcMenu->addMenu(tr("PAL"));
	m_palMenu->addAction(m_loadPALAct);
	m_palMenu->addAction(m_savePALAct);
}

void Hokuto::MapWindow::readSettings()
{
	QSettings settings("DrillCity", "Hokuto");
	QPoint winpos = settings.value("map_pos", QPoint(0, 0)).toPoint();
	QSize winsz = settings.value("map_sz", QSize(320, 224)).toSize();
	move(winpos);
	resize(winsz);
}

void Hokuto::MapWindow::writeSettings()
{
	QSettings settings("DrillCity", "Hokuto");
	settings.setValue("map_pos", pos());
	settings.setValue("map_sz", size());
}

void Hokuto::setModified()
{
	g_modified = true;
	g_mapwin->setWindowModified(g_modified);
}

void Hokuto::setUnmodified()
{
	g_modified = false;
	g_mapwin->setWindowModified(g_modified);
}
