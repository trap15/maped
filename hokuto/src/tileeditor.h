#ifndef TILEEDITOR_H
#define TILEEDITOR_H

//--- Qt includes
#include <QtGui>
#include <QtWidgets>

#include "gamedef.h"

namespace Hokuto {
	class TileEditor : public QWidget
	{
		Q_OBJECT

	public:
		explicit TileEditor(QWidget *parent = 0);

		int m_sel;
		void updateBox();

	protected:
		void paintEvent(QPaintEvent *event);
		void mousePressEvent(QMouseEvent *event);

	protected slots:
		void applyTile();
		void valueChange(int i);
		void stateChange(int i);

	private:
		QGridLayout* m_grid;

        QLabel *m_lbl_til[TILE_SIZE];
        QSpinBox *m_spn_til[TILE_SIZE];
        QLabel *m_lbl_pal;
        QSpinBox *m_spn_pal;

        QLabel *m_lbl_attr[8];
        QCheckBox *m_chk_attr[8];
	};

	extern TileEditor *g_tiled;
}

#endif // TILEEDITOR_H
