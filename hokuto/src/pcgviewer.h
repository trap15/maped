#ifndef PCGVIEWER_H
#define PCGVIEWER_H

//--- Qt includes
#include <QtGui>
#include <QtWidgets>

namespace Hokuto {
	class PCGViewer : public QWidget
	{
		Q_OBJECT

	public:
		explicit PCGViewer(QWidget *parent = 0);

	protected:
		void paintEvent(QPaintEvent *event);

	private:
		int m_pal;
	};

    extern PCGViewer *g_pcgvw;
}

#endif // PCGVIEWER_H
