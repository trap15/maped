#ifndef MAP_H
#define MAP_H

namespace Hokuto {
	struct Tile;
	struct MapTile
	{
		MapTile();

        uint8_t m_flags[TILE_ATTR_SIZE];
        Tile m_tile[TILE_SIZE];
	};

	struct Map
	{
		Map();

		void reset();
		void changeSize(int w, int h);
		void scroll(int x, int y);
		void setScroll(int x, int y);

		bool loadMAP(QString path);
		bool loadMAP(void *data);

		bool saveMAP(QString path);
		bool saveMAP(uchar* &data, qint64 &len);

		void update();
		// Image used for overlays (for visible heights, etc.)
		QImage *getImage();

		int m_xscroll, m_yscroll;
		int m_w, m_h;
		int m_xb, m_yb;
		MapTile m_tiles[256];
		uint8_t *m_map;

	private:
		void _draw_flags();
		void _draw_grid();
		void _update_screen();

		QImage m_img;
	};

	extern Map g_map;
}

#endif // MAP_H
